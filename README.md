EMB1 - Robot Electronics
====
University of Southern Denmark

Course Description
==================

Knowledge
---------
* Analogue / digital electronic interfaces
* Sensors and actuators
* Input/output devices
* Microprocessors
* Communication between electronic devices
* State machines
* FPGA
* VHDL
* Communication interfaces

Skills
------
* Control an actuator
* Use relevant analogue and digital interfaces for sensors and actuators
* Develop the interface between analogue and digital electronics.

Competences
-----------
* Develop a functional system combining knowledge and experience from the course theory and practice.
* Document technical results
