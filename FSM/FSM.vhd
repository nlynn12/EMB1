----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/10/2015 09:16:14 AM
-- Design Name: 
-- Module Name: vhd - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm is
    Port ( 
        CLK     : in STD_LOGIC;                     -- 50 MHz Clock used for the sequential processes
        rst     : in STD_LOGIC;                     -- Reset pin
        EXT_LED : out STD_LOGIC_VECTOR(5 downto 0); -- The 6 external LEDs arranged in a vector
        input   : in STD_LOGIC                      -- Input pin 
        );
end fsm;

architecture behavioral of fsm is
    type state_type is (s0,s1,s2,s3,s4,s5);
    signal cr_state, nx_state : state_type;
    signal LED_BUS          : STD_LOGIC_VECTOR(5 downto 0) := "000000"; -- Bus holding the six external LEDs
    signal debounced_res    : STD_LOGIC;                                -- Debounced input from debounce module
    
    -- declaration of debounce module
    component debounce is
        port (
            CLK     : in    STD_LOGIC;
            input   : in    STD_LOGIC;
            res     : out   STD_LOGIC);
    end component;
    
begin
    -- positional port mapping
    debounce_mapping : debounce
    port map(CLK, input, debounced_res);    -- (CLK => CLK, input => input, debounced_res => res)

    process (CLK,rst)
        begin
            if(rst = '1') then
                -- default state on reset
                cr_state <= s0;              
            elsif(rising_edge(CLK)) then
                -- state change
                if(cr_state /= nx_state) then
                    cr_state <= nx_state;
                end if;
            end if;  
    end process;
    
    -- state machine process
    process(debounced_res, cr_state)    
        begin
        if (rising_edge(debounced_res)) then
            case cr_state is
                when s0 =>
                    LED_BUS <= "000001";
                    nx_state <= s1;
                when s1 =>
                    LED_BUS <= "000010";                   
                    nx_state <= s2;
                when s2 =>
                    LED_BUS <= "000100"; 
                    nx_state <= s3;   
                when s3 =>
                    LED_BUS <= "001000";
                    nx_state <= s4;
                when s4 =>
                   LED_BUS <= "010000";
                    nx_state <= s5;
                when s5 =>
                    LED_BUS <= "100000";
                    nx_state <= s0;
            end case;
         end if;
    end process;
        
    EXT_LED <= LED_BUS;
    
end Behavioral;