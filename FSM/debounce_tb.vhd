----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/17/2015 10:37:39 AM
-- Design Name: 
-- Module Name: debounce_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debounce_tb is
end debounce_tb;

architecture Behavioral of debounce_tb is
    component debounce is
        port (
            CLK     : in    STD_LOGIC;
            input   : in    STD_LOGIC;
            res     : out   STD_LOGIC);
    end component;
    
    -- signals
    signal CLK      : STD_LOGIC := '0';
    signal input    : STD_LOGIC := '0';
    signal res      : STD_LOGIC := '0';
    
    -- Clock
    constant CLK_period : time := 20ns;
    
begin
    UUT : debounce port map(CLK=>CLK, input=>input, res=>res);
    
    CLK_LABEL : process
    begin
        CLK <= '0';
        wait for CLK_period/2;
        CLK <= '1';
        wait for CLK_period/2;
    end process;
    
    DEBOUNCE_PROCESS : process
    begin
        for I in 1 to 3 loop
            input <= '0';
            wait for 9ms;
            input <= '1';
            wait for 9ms;
        end loop;
        input <= '1';
        wait for 20ms;
    end process;
    

end Behavioral;
