----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/18/2015 08:52:44 AM
-- Design Name: 
-- Module Name: fsm_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_tb is
end fsm_tb;

architecture Behavioral of fsm_tb is
    component fsm is
        Port ( 
            CLK     : in STD_LOGIC;                     -- 50 MHz Clock used for the sequential processes
            rst     : in STD_LOGIC;                     -- Reset pin
            EXT_LED : out STD_LOGIC_VECTOR(5 downto 0); -- The 6 external LEDs arranged in a vector
            input   : in STD_LOGIC                      -- Input pin 
        );
    end component;

    -- signals
    signal CLK      : STD_LOGIC := '0';
    signal rst      : STD_LOGIC := '0';
    signal EXT_LED  : STD_LOGIC_VECTOR(5 downto 0) := "000000";
    signal input    : STD_LOGIC := '0';
    
    -- Clock
    constant CLK_period : time := 20ns;
    
begin

    UUT : fsm port map(CLK=>CLK, rst=>rst, EXT_LED=>EXT_LED, input=>input);
    
    CLK_LABEL : process
    begin
        CLK <= '0';
        wait for CLK_period/2;
        CLK <= '1';
        wait for CLK_period/2;
    end process;
    
    FSM_PROCESS : process
    begin
        for I in 1 to 3 loop
            input <= '0';
            wait for 1ms;
            input <= '1';
            wait for 1ms;
        end loop;
    end process;
end Behavioral;
