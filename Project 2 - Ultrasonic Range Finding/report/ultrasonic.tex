\section{Ultrasonic Range Finding in General}
Ultrasonic sound is sound waves at frequencies higher than 20kHz\footnote{ANSI Standard}, which is also the approximate upper bound for what humans can hear. Ultrasonic sound, also refered to as sonar, is widely used in various range finding applications where underwater range finding is one of the most commenly known. In general, using ultrasonic sound for distance measurements is done by generating an ultrasonic pulse in a static direction. If any object lies in the path of the pulse, the sound will be reflected by this and an echo will return to the receiver. The time it has taken for the sound to travel to the object and back again is then used, together with the speed of sound, in order to compute the distance.

\section{Automatic Calibration of the Sonar Range Scanner Considering The Speed of Sound through Air in Environments with Varying Climate Properties}
The speed of sound through air varies depending on a set of parameters, which can be obtained from the kinetic theory of gases. Due to this fact, it is necessary to construct an automatic calibration for the sensor, when it is to be used in environments with varying climate properties. These properties are the parameters obtained from the kinetic theory of gases; pressure and temperature. In the theory of gases, the pressure is a result of collisions between the atoms and the container walls, where the temperature is a result of the atoms' kinetic energy.\\

\noindent The speed of sound can in general be determined by the Newton--Laplace formula in equation \eqref{eq:newton-laplace-speed-sound}. 
\begin{equation}
 c = \sqrt{\frac{K_s}{\rho}}
 \label{eq:newton-laplace-speed-sound}
\end{equation}
where $K_s$ is the isentropic bulk modulus. The bulk modulus is in general a measure of a substance's resistance to uniform compression and for ideal gasses it can be computed by:
$$K_s = \gamma p$$
However as air is a composite of several gasses, this equation cannot be used in determining the isentropic bulk modulus of air. Instead, assume that the process of measuring the environment does not transfer heat to its surroundings, then the process is adiabatic. In that case, the bulk modulus is also adiabatic, which means that it is simply a constant that equals to:

$$K_{\text{adiabatic}} = 1.42\cdot10^5 \text{Pa}$$

\noindent Therefore, the only unknown coefficient in equation \eqref{eq:newton-laplace-speed-sound} is $\rho$, which is the density. The density of humid air can be determined by equation \eqref{eq:air-density}.
\begin{equation}
 \rho_{\text{humid air}} = \frac{p_d}{R_d T} + \frac{p_v}{R_v T} = \frac{p_d M_d + p_v M_v}{RT}
 \label{eq:air-density}
\end{equation}
where:
\begin{table}[H]
\begin{tabular}{ll}
  $\rho_{\text{humid air}}$ & \text{Density of the humid air [kg/m$^3$]}\\
  $p_{d}$                   & \text{Partial pressure of dry air [Pa]}\\
  $R_{d}$                   & \text{Specific gas constant for dry air, 287.058 [J/(kg$\cdot$K)]}\\
  $T    $                   & \text{Temperature [K]}\\
  $p_{v}$                   & \text{Pressure of water vapor [Pa]}\\
  $R_{v}$                   & \text{Specific gas constant for water vapor, 461.495 [J/(kg$\cdot$K)]}\\
  $M_{d}$                   & \text{Molar mass of dry air, 0.028964 [kg/mol]}\\
  $M_{v}$                   & \text{Molar mass of water vapor, 0.018016 [kg/mol]}\\
  $R    $                   & \text{Universal gas constant, [8.314 J/(K$\cdot$mol)]}\\
\end{tabular}
\end{table}

\noindent The vapor pressure, $p_v$, can be computed from the saturation vapor pressure, $p_{\text{sat}}$, and the relative humidity, $\phi$:
$$p_v = \phi p_{\text{sat}}$$
Here the relative humidity will be read by a humidity sensor and the saturation vapor pressure can be computed for any given temperature when the relative humidity is 100\%, see equation \eqref{eq:sat_vapor_pressure}.
\begin{equation}
 p_{\text{sat}} = 6.1078 \cdot 10^{\frac{7.5T}{T+237.3}}
 \label{eq:sat_vapor_pressure}
\end{equation}
Note that in this equation the temperature is measured in degrees Celsius. The results of this equation yields the pressure in hPa.\\

\noindent Lastly the partial pressure of dry air, $p_d$ can be determined by simply subtracting the vapor pressure from the observed absolute pressure, $p$.
\begin{equation}
 p_d = p - p_v
 \label{eq:partial_pressure_dry_air}
\end{equation}
where the absolute pressure is a zero--referenced measurement against perfect vacuum. This means that it equals the sum of the gauge pressure and the atmospheric pressure.\\

\noindent Now there is no more unknowns in equation \eqref{eq:air-density}.

\section{Initial Calculations of the Speed of Sound varying the Air Characteristics}\label{sec:initial_temp_calc}
Before selecting the sensors for the calibration module, it is necessary to determine how the three parameters impacts the speed of sound. The formulas used for these initial computations of the speed of sound are implemented in a Matlab function called \lstinline|amb2prop()| developed by Vicente Cutanda Henriquez (former employee at SDU). V. Cutanda's development is based on a paper published by Knud Rasmussen from SDU\footnote{\cite{amb2prop}}. The Matlab script allows for specification of sound frequency, temperature, pressure and humidity. As the frequency of the ultrasonic module is constant, it is only the three remaining variables that are plotted below. In figure \ref{fig:air} three plots are shown, in which the three air characteristics are varied and plotted against their respective speeds of sound.
\vspace*{-8pt}
\begin{figure}[H]
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=1.1\textwidth]{img/t_c}
        \caption{This plot shows the speed of sound as a function of temperature}
    \end{subfigure}
    ~ 
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=1.1\textwidth]{img/h_c}
        \caption{This plot shows the speed of sound as a function of humidity}
    \end{subfigure}
    
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=1.1\textwidth]{img/p_c}
        \caption{This plot shows the speed of sound as a function of pressure}
    \end{subfigure}
    \caption{The three figures show the how the three parameters of air affect the speed of sound. In (a) and (b) it is shown that the relationships are linear while in (c) the speed of sound decays exponentially}
    \label{fig:air}
\end{figure}
\noindent It can be seen from the three graphs that the speed of sound varies mostly by varying the temperature. Therefore, it is of interest to have an accurate temperature sensor. Hereby the average change in speed of sound for a change $\Delta$T = 0.5s is computed, which equals 0.3192m/s. This is the estimated mean from the data computed with the \lstinline|amb2prop()| function. Assume now that the temperature is measured at 20$^\circ$C, but is actually 20.5$^\circ$C. Then the speed of sound at 50\% humidity and 101,325 Pa atmospheric pressure is computed to be 343.9862m/s where the true speed of sound would be $\approx$ 344.3054m/s. If an ultrasonic wave is sent out and the time it takes for the echo to return to the sensor equals 60$\mu$s, then the distances calculated will differ with 
$$10.3196\cdot 10^{-3}m - 10.3292\cdot 10^{-3}m = -9.6 \cdot 10^{-6}m$$
which is deemed highly accurate as the desired accuracy in metrics is $\pm$1mm.

\section{Selecting Components for Air Characteristics Measurements}
For measuring the parameters needed in the calibration of the speed of sound, three sensors are required; a temperature, a relative humidity and an absolute pressure sensor. The accuracy of these sensors determine the accuracy at which the speed of sound can be determined. An initial inspection of the applications in which the sensors are to be used results in the following criteria for the sensors.

\paragraph{The temperature sensor} should be able to measure temperatures from -25$^\circ$C to +50$^\circ$C, while the accuracy should be at least $\pm 0.5^\circ$. The accuracy is determined from initial calculations of the speed of sound varying the temperature only as seen in section \ref{sec:initial_temp_calc}. The result of this is that a somewhat significant change happens at a $\Delta T = \pm0.5^\circ$C. Furthermore, it is desired that the temperature sensor by factory default is calibrated to centigrade with a linear scale factor. Lastly it is needed that the temperature sensor has a relatively low self--heating, as it is assumed that the ultrasonic range finding sensor does not transfer heat to its surroundings.

\paragraph{The humidity sensor} should be able to measure the relative humidity of air from 5\% to 95\%.

\paragraph{The pressure sensor} should be able to measure the \textbf{absolute} pressure in accordance with equation \eqref{eq:partial_pressure_dry_air}. It should furthermore be able to measure pressures at high altitudes.\\

\noindent The sensors selected from the above criteria are the:
\begin{itemize}
 \item LM35DZ - Precision Centigrade Temperature Sensor\footnote{\cite{TI_temp}}
 \item HS1101LF - Relative Humidity Sensor\footnote{\cite{MS_humidity}}
 \item MPXH6250AC6U - 20 to 250 kPa, Absolute Pressure Sensor\footnote{\cite{FS_pressure}}
\end{itemize}

The temperature sensor can measure temperature in centigrades and outputs this linearly. The accuracy is ensured to be $\pm0.5^\circ$C at 25$^\circ$C. The relative humidity sensor is selected based on the capability to output the relative humidity of air from 0\% to 100\%. Finally, the pressure sensor is selected based on the ability to measure the absolute pressure in a high range from 20kPa to 250kPa, which corresponds to a range in atm from 0.197384653 atm to 2.46730817 atm.

\section{External Non--Automatic Calibration of the Sonar Range Scanner for Stable Environments}
If the sonar range scanner is to always be used in a stable environment, then a cheaper version of the sonar range scanner can be constructed by leaving out the three calibration sensors, while then adding the ability to set the variable speed of sound to a fixed estimate through UART communication. The UART communication protocol is integrated in the ATmega328p and it is implemented in the embedded C application, merely as a debugging utility, outputting values from the sensors. The UART interface enables easy user access through a USB connector.

\section{Choosing a Controller Unit for the Sonar Range Scanner}
In order to compute the speed of sound and to control the motor and the ultrasonic module, a controller unit is needed. The controller unit is chosen to be a microcontroller due to their low price. The microcontroller has to meet some requirements listed below.

\begin{itemize}
 \item Communication interface (SPI/I2C/etc.)
 \item At least 3 timers for: 
 \begin{itemize}\itemsep-3pt
  \item PWM generation to the DC motor
  \item System clock (control loop)
  \item Frequency measurement of the humidity sensor output
 \end{itemize}
 \item At least 2 interrupt pins for:
 \begin{itemize}\itemsep-3pt
  \item Encoder circuit
  \item Frequency measurement of the humidity sensor output\\\textbf{Note:} only applicable if linear output does not work
 \end{itemize}
 \item Programmable in C
\end{itemize}
It is furthermore desired that the microcontroller has a built-in A/D converter due to physical space. However, the price must not comprimise this desired requirement.\\

\noindent The selected microcontroller is Atmel's ATmega328p\footnote{\cite{Atmel_ATmega328p}} which meets all of the above requirements. It also has a built--in ADC which actually proved cheaper than buying the two components seperately. If the microcontroller did not have a built--in ADC, the MCP3008\footnote{\cite{microchip_MCP3008}} would have been the chosen ADC due to its simple communication interface (SPI) and its ability to sample from more than one input by multiplexing.

\begin{comment}

\newpage
\begin{table}[H]
\begin{tabular}{lll}
\toprule
\textbf{Temperature}    &      \textbf{Speed of sound}     &    \textbf{Density of air} \\
\midrule
35	       &      351.88	         &    1.1455         \\
30	       &      349.02	         &    1.1644         \\
25	       &      346.13	         &    1.1839         \\
20	       &      343.21	         &    1.2041         \\
15	       &      340.27	         &    1.2250         \\
10	       &      337.31	         &    1.2466         \\
5	       &      334.32	         &    1.2690         \\
0	       &      331.30	         &    1.2922         \\
-5	       &      328.25	         &    1.3163         \\
-10	       &      325.18	         &    1.3413         \\
-15	       &      322.07	         &    1.3673         \\
-20	       &      318.94	         &    1.3943         \\
-25	       &      315.77	         &    1.4224         \\
\bottomrule
\end{tabular}

\end{table}

\subsection*{Components:}
\begin{itemize}\itemsep-3pt
 \item Temperature sensor\\ \url{http://dk.rs-online.com/web/p/temperatursensorer-og-fugtighedssensorer/5335907/}
 \item Humidity sensor\\ \url{http://dk.rs-online.com/web/p/temperatursensorer-og-fugtighedssensorer/8937082/}
 \item Absolute Air Pressure \\ \url{http://dk.farnell.com/webapp/wcs/stores/servlet/Search?st=MPL115A&catalogId=15001&categoryId=700000004367&langId=45&storeId=10157}
 \item Absolute Air Pressure sensors:
 \begin{itemize}
  \item Atmospheric pressure sensor (Relative/Barometric Air Pressure (BAP)) sensor \\ \url{https://www.sparkfun.com/products/11824}
  \item 
 \end{itemize}
\end{itemize}


\subsection*{Sources:}
\begin{itemize}\itemsep-3pt
 \item \url{https://en.wikipedia.org/wiki/Speed_of_sound}
 \item \url{https://en.wikipedia.org/wiki/Density_of_air}
 \item \url{https://en.wikipedia.org/wiki/Bulk_modulus}
 \item \url{https://en.wikipedia.org/wiki/Pressure_measurement#Absolute.2C_gauge_and_differential_pressures_-_zero_reference}
 \item \url{https://en.wikipedia.org/wiki/Atmospheric_pressure}
 \item \url{https://www.grc.nasa.gov/www/k-12/airplane/airprop.html}
\end{itemize}
\end{comment}