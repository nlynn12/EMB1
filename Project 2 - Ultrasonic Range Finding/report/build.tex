\section{Sonar Module}
The sonar module consists of a circuit driving the transmitter and a circuit which amplifies the voltage outputted by the receiver.

\subsection{Transmitter}
The chosen transmitter is the 400PT16P\footnote{\cite{multicomp_transmitter}}. It has a nominal frequency of $40$kHz and a beam angle of $30^\circ \pm 5^\circ$\footnote{\cite{multicomp_transmitter} p. 1}. A beam angle this wide is less than optimal. As the sonar module is to be rotated, a narrow beam angle is desired, in order to achieve the optimum resolution. However, due to budget reasons, it has been chosen anyway. The datasheet states no maximum input voltage. Since $5$V are readily available due to being used elsewhere in the system, it has been chosen to use an amplifying H--bridge circuit, which doubles the voltage. This means that the transmitter will be powered by $10$V. For the H--bridge, two 2N3904\footnote{\cite{STMicroelectronics_2N3904}} NPN transistors and two BC257B\footnote{\cite{ME_BC257B}} PNP amplifier transistors have been used, along with another 2N3904 transistor used to invert one of the H--bridge inputs. The H--bridge will be controlled using a PWM signal in order to determine the frequency. The circuit used can be seen on figure \ref{fig:transmitterCircuit} below:

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{img/transmitterCircuit}
\caption{Eagle$^{\scriptsize{\textregistered}}$ schematic of the amplifying H--bridge driving the transmitter.}
\label{fig:transmitterCircuit}
\end{figure}

\subsection{Receiver}
The receiver used for the project is the MA40S4R\footnote{\cite{muRata_receiver}}. It has a nominal frequency of $40$kHz and a detectable range of $0.2$ to $4$ meters\footnote{\cite{muRata_receiver} p. 1}, where the Hokuyo URG-04LX has a range of $0.02$ to $5.4$ meters, which makes the two sensors comparable in that aspect. If the AGV on which the sonar range scanner is to be placed has a diameter of more than $0.2$ meters, this will not impend any problems. However, if the module is to be placed on an AGV with a smaller diameter than $0.2$ meters, precautions are to be taken.\\
The voltage outputted by the receiver is very weak and can be down to less than $1$mV. Therefore, an amplifier with a high gain is needed. The chosen circuit consists of to op--amps and can be seen on figure \ref{fig:receiverCircuit}. The first op--amp sub--circuit is a non--inverting AC coupled amplifier, which has been biased at half the input voltage ($2.5$V). It can be seen that ground for the positive input of the op--amp has been replaced with the bias voltage, while ground for the negative input has not, as this only connected to a capacitor and a resistor in series, and capacitors cannot pass DC signals.%\footnote{\cite{MIT_howtobias} p. 3}. 
In addition, the supply ground for the op--amp has not been replaced either, as this does not interfere with the input. The input signal from the receiver is amplified by the feedback resistor. The output of the non--inverting amplifier is fed to a comparator, which has a reference voltage of half the supply voltage. This way, a square signal is output from the receiver circuit whenever the output of the first op--amp exceeds the $2.5$V bias.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{img/receiverCircuit}
\caption{Eagle$^{\scriptsize{\textregistered}}$ schematic of the receiver circuit.}
\label{fig:receiverCircuit}
\end{figure}

\section{Rotation Module}
The fundamental idea behind this project is to rotate the sonar module. In order to do this, a DC--motor with an encoder along with a slip ring are to be used. The motor will be powering the slip ring by way of gears or a belt. On the slip ring, an encoder disc will be placed. The designed encoder disc can be seen on figure \ref{fig:encoderDisc} and has 36 windows, which yields a precision of $10^\circ$ for the sonar module.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{img/encoder_disc}
\caption{Drawing of the designed encoder disc.}
\label{fig:encoderDisc}
\end{figure}

To the encoder disc, a position encoder like the MOC7811 will be placed, in order to detect the windows. A sketch of the system can be seen on figure \ref{fig:rotationSketch}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{img/rotating}
\caption{Sketch of the rotating system.}
\label{fig:rotationSketch}
\end{figure}

\section{Calibration Module}
As stated in chapter 2, the speed of sound is based on three factors; humidity, pressure and temperature. Therefore, in order to always get the most accurate readings from the sonar range scanner, a calibration module will be made. This module will consist of three sensors for detection of said variables. The sensor circuits will be discussed in the following subsections.

\subsection{Humidity}
In order to measure the humidity of the air, an HPP801A031\footnote{\cite{MS_humidity}} relative humidity sensor has been chosen. It has a supply voltage of maximum $10$V and can detect from $1$\% to $99$\% relative humidity. It requires no calibration and works by changing capacitance based on the air humidity. The circuit chosen for running this sensor is the one recommended in the datasheet\footnote{\cite{MS_humidity} p. 3}.% and can be seen on figure \ref{fig:humidityCircuit}.

%\missingfigure[figwidth=6cm]{Sketch of rotating sonar module with DC motor and slip ring.}\label{fig:humidityCircuit}

The circuit is an astable 555 timer circuit and works by charging and discharging the capacitor. The time it takes for this to happen is what defines the frequency of the output. By measuring this frequency, the humidity can be found. In the datasheet\footnote{\cite{MS_humidity} p. 4}, the following lookup table is seen:

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l}
\hline
\textbf{RH(\%)}         & \textbf{0}  & \textbf{5}  & \textbf{10} & \textbf{15} & \textbf{20} & \textbf{25} & \textbf{30} & \textbf{35} & \textbf{40} & \textbf{45}  & \multicolumn{1}{l|}{\textbf{50}} \\ \hline
\textbf{F$_\text{out}$} & -           & -           & 7155        & 7080        & 7010        & 6945        & 6880        & 6820        & 6760        & 6705         & \multicolumn{1}{l|}{6650}        \\ \hline
\textbf{RH(\%)}         & \textbf{55} & \textbf{60} & \textbf{65} & \textbf{70} & \textbf{75} & \textbf{80} & \textbf{85} & \textbf{90} & \textbf{95} & \textbf{100} &                                  \\ \cline{1-11}
\textbf{F$_\text{out}$} & 6600        & 6550        & 6500        & 6450        & 6400        & 6355        & 6305        & 6260        & 6210        & -            &                                  \\ \cline{1-11}
\end{tabular}
\caption{Typical response lookup table.}
\end{table}

\noindent This circuit has been build and tested to work.

\subsection{Pressure}
The chosen sensor for pressure measurement is the MPXH6250AC6U\footnote{\cite{FS_pressure}}, which measures absolute pressure. It is supplied by $5$V and outputs from $0$V to $5$V based on a pressure reading from $20$kPa to $250$kPa. The datasheet\footnote{\cite{FS_pressure} p. 5} recommends the following circuit, figure \ref{fig:pressureCircuit}, for power supply decoupling and output filtering.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{img/pressureCircuit}
\caption{Eagle$^{\scriptsize{\textregistered}}$ schematic of the supply decoupling and output filtering pressure sensor circuit.}
\label{fig:pressureCircuit}
\end{figure}

The pressure circuit has a linear output correlating with the measured pressure level. This circuit has been built and tested to work.

\subsection{Temperature}
The LM35DZ\footnote{\cite{TI_temp}} centigrade temperature sensor has been chosen for temperature measurement. It has a precision of $0.4^\circ$\footnote{\cite{TI_temp} p. 1}. When measuring from $2^\circ$C to $150^\circ$C, nothing but supplying the sensor with between $4$V and $20$V is required, as seen on figure \ref{fig:temperatureCircuit}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{img/temperatureCircuit}
\caption{Eagle$^{\scriptsize{\textregistered}}$ schematic of the temperature sensor circuit.}
\label{fig:temperatureCircuit}
\end{figure}

\noindent This circuit has been build and tested.