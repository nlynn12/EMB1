Designing autonomous mobile robot systems introduces several problems, where obstacle avoidance is of high priority. In order to avoid obstacles, they need to be detected before impact. In many cases, this is done by laser range scanners, which are typically expensive. This project seeks to create a low--cost substitute for the laser range scanner. This will be done by exploring the possibilities of using ultrasonic sound instead of light. Given this, an ultrasonic sound module will be rotated in the same manner as a laser range scanner, in order to create a representation of the scene in which the sensor is placed. For the rest of the report, this will be referred to as a sonar range scanner. In figure \ref{fig:sector_robot} an illustration of the above is shown, where the sensor detects two obstacles in the scene.

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.40\textwidth}
        \hspace*{1.2cm}\vspace*{2.3cm}\includegraphics[width=0.5\textwidth]{img/conceptDrawingTop}
        \caption{Environment in which the sonar range scanner is placed in the center and where two obstacles are detected by the sensor.}
        \label{fig:robot_environment}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.55\textwidth}
        %\hspace{1cm} 
	\begin{tikzpicture}[scale=0.6]
	\drawaxes[diagram angle=15,diagram radius=7,draw axes]{};
	\sectorlist[sector angle=30,
	draw sector list={
	  6cm/green,
	  6cm/green,
	  6cm/green,
	  3cm/red,
	  6cm/green,
	  6cm/green,
	  6cm/green,
	  6cm/green,
	  6cm/green,
	  6cm/green,
	  3cm/red,
	  3cm/red}]{};
	\end{tikzpicture}
        \caption{Illustration of a sector diagram for distance measurements created by the sonar range scanner. Here, the resolution of the sonar sensor is set to 30$^{\circ}$ for illustration purposes.}
        \label{fig:illustration_sector_diagram}
    \end{subfigure}
    \caption{The two illustrations represents (a) the sonar range scanner in its environment and (b) the representation of the environment based on the distance measurements conducted by the sonar range scanner with a resolution of 30$^\circ$.}\label{fig:sector_robot}
\end{figure}
\noindent Throughout the report, comparisons to the Hokuyo laser range scanner will be performed in order to visualize the differences in resolution and accuracy.\\

\noindent The primary goal of this project is to design a sonar range scanner to be used for obstacle avoidance. The target devices are autonomous guided vehicles (AGV's). As the speed of sound varies depending on a set of properties for air, this project seeks to investigate how these properties can be automatically sensed and taken into account. This means that the system will calibrate itself depending on the environment in which it is placed, which enables the use of this sensor in scenes with varying climates.\\

%\noindent Typically, an ultrasonic sensor is fixated, pointing in a fixed direction relative to the robot. Sound is then transmitted by a speaker and a timer is started. When the echo of the sound is received by the microphone, the timer stops. The time it took for the sound to travel, hit an object and return is then used to estimate the distance. In this project, the sensor will be rotated 360 degrees continuously, in order to generate a mapping of the unknown environment. This might result in some hard requirements for the timing of the rotations, as the echo of the sound transmitted by the speaker needs to return to the microphone before the sensor faces in another direction.\\

\noindent Lastly, this project will not concern the development of a mobile robot due to the project's limited time period. This is not a necessary part of proving the concept.

\section{Component Overview of the entire System}
In figure \ref{fig:complete_sys_block_diagram} a complete overview of the system is shown. It contains relatively few components, as the main focus will lie in the development of the sonar range scanner.
\begin{figure}[h!]
 \centering
 \begin{tikzpicture}[
    >=stealth',
 ]
 \tikzset{
    connect/.style args={(#1) to (#2) over (#3) by #4}{
        insert path={
            let \p1=($(#1)-(#3)$), \n1={veclen(\x1,\y1)}, 
            \n2={atan2(\y1,\x1)}, \n3={abs(#4)}, \n4={#4>0 ?180:-180}  in 
            (#1) -- ($(#1)!\n1-\n3!(#3)$) 
            arc (\n2:\n2+\n4:\n3) -- (#2)
        }
    },
}
 
 
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(0,3) (0) {PC};
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(0,0) (1) {FPGA};
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm, very thick] at(5,0) (2) {Microprocessor};
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(10,-1) (3) {Microphone};
  
  %\node[draw, rectangle, minimum width=2cm, minimum height=1cm] at(10,0) (4) {ADC};
  
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(10,-3.3) (5) {Motor};
  %\node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(10,-4) (6) {LED Driver};
  
  %\node[draw, rectangle, minimum width=3cm, minimum height=2.5cm] at(0,-4) (7) {Power Supply};
  
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(10,1) (8) {Speaker};
  
  \node[draw, rectangle, minimum width=2cm, minimum height=1cm] at(10,-4.8) (9) {Encoder};
  \node[draw, rectangle, minimum width=3.6cm, minimum height=3.3cm, dashed] at(10,-3.9) { };
  \node[draw, rectangle, minimum width=3.6cm, minimum height=4.4cm, dashed] at(10,0.2) { };
  \node at(10,2.1) {Ultrasonic Module};
  
  \node[draw, rectangle, minimum width=2.5cm, minimum height=1.5cm, align=center] at(4.75,4.5) (10) {Temperature\\sensor};
  \node[draw, rectangle, minimum width=2.5cm, minimum height=1.5cm, align=center] at(7.50,4.5) (11) {Humidity\\sensor};
  \node[draw, rectangle, minimum width=2.5cm, minimum height=1.5cm, align=center] at(10.25,4.5) (12) {Pressure\\sensor};
  \node[draw, rectangle, minimum width=8.5cm, minimum height=2.5cm, dashed] at(7.5,4.65) { };
  \node at(7.5,5.6) {Calibration Sensors};
  %\draw[->] (5.25,3.25) -- (5.25,0.75);
  %\draw[->] (2.5,3.25) -- (2.5,2.5) -- (5,2.5) -- (5,0.75);
  %\draw[->] (-0.25,3.25) -- (-0.25,2) -- (4.75,2) -- (4.75,0.75);
 
  \draw[->] (12) -- (10.25,2.8) -- (5.25,2.8) -- (5.25,0.75);
  \draw[->] (11) -- (7.5,3) -- (5,3) -- (5,0.75);
  \draw[->] (10) -- (4.75,0.75);
  
  
  \node[draw, rectangle, minimum width=9cm, minimum height=12.5cm] at(7.5,0.5) { };
  \node at(7.5,6.4) {\Large{Sonar Range Scanner}};
  
  % PC -> FPGA
  \draw[->] (-0.2,2.25) -- (-0.2,0.75) node[above,midway] { };
  \draw[<-] (0.2,2.25) -- (0.2,0.75) node[above,midway] { };
  %\draw[<-] (1.5,-0.2) -- (3.5,-0.2) node[above,midway] { };
  
  % FPGA -> uC
  %\draw[<-] (1) -- (2) node[above,midway] { };
  \draw[->] (1.5,0.2) -- (3.5,0.2) node[above,midway] { };
  \draw[<-] (1.5,-0.2) -- (3.5,-0.2) node[above,midway] { };
  % uC -> Speaker
  \draw[->] (6.5,0.25) -- (7.5,0.25) -- (7.5,1)-- (8) node[above,midway] { };
  %uC -> Motor
  \draw[->] (5.25,-0.75) -- (5.25,-3.3)--(8.5,-3.3) node[above,midway] { };
  % Encoder -> uC
  \draw[->] (9,-4.8) -- (4.75,-4.8) -- (4.75,-0.75);
  % Microphone -> uC
  \draw[->] (3) -- (7.5,-1) -- (7.5,-0.25)-- (6.5,-0.25) node[above,midway] { };
\end{tikzpicture}
\caption{Block diagram of the complete system.}\label{fig:complete_sys_block_diagram}
\end{figure}

As seen in figure \ref{fig:complete_sys_block_diagram} the sonar range scanner consists of 3 modules and a microprocessor. The first module is the motor which will be a simple DC-motor with an integrated encoder. The motor will continuously rotate the sonar module, which consists of a transmitter and a receiver, therefore a slip ring is needed between the motor and the sonar module. The next module contains the calibration sensors which are used to compute the speed of sound. The final module is the microprocessor, which will be responsible for; (1) sampling from the calibration sensors, (2) turning the sonar module, (3) emitting sound waves from the sonar module and lastly (4) computing the actual distance from the sensor to the nearest object in the direction that the sonar module faces.

\subsection{Interfacing the S3XB FPGA to the Sonar Range Scanner}
The interface between the S3XB FPGA and the Sonar Range Scanner will be SPI as of its high throughput rates compared to e.g. I2C. The SPI interface needs at least two additional wires compared to the I2C's two wire connection, however this is deemed irrelevant as there is a large number of GPIO's available on both the FPGA and the microprocessor.

\subsection{Interfacing the S3XB FPGA to a PC}
The $\mu$TosNet protocol developed at the University of Southern Denmark is used in this project for interfacing the FPGA with PCs. Its main feature will be as a debugging utility. However, it will also be used for setting the resolution of the sonar range scanner from the PC.

\section{Automatic vs. Non-Automatic Calibration of the Sonar Range Scanner}
As described above, the sonar range scanner will be developed as an automatically calibrating sensor. However, the methods and computations described regarding this calibration could easily be executed externally if the environment in which the sensor is placed is stable. The values of these calibration computations should then be send to the controlling circuit, the S3XB FPGA, which can be communicated with by utilizing the $\mu$TosNet protocol. In this case, the complete overview of the system, seen in figure \ref{fig:complete_sys_block_diagram}, reduces in the sense that the calibration sensors are removed. The values obtained by these sensors are however still essential to know in order to do the external calibration.

\begin{comment}
\begin{figure}[h!]
 \centering
 \begin{tikzpicture}[
    >=stealth',
 ]
 \tikzset{
    connect/.style args={(#1) to (#2) over (#3) by #4}{
        insert path={
            let \p1=($(#1)-(#3)$), \n1={veclen(\x1,\y1)}, 
            \n2={atan2(\y1,\x1)}, \n3={abs(#4)}, \n4={#4>0 ?180:-180}  in 
            (#1) -- ($(#1)!\n1-\n3!(#3)$) 
            arc (\n2:\n2+\n4:\n3) -- (#2)
        }
    },
}
 
 
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(0,3) (0) {PC};
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(0,0) (1) {FPGA};
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm, very thick] at(5,0) (2) {Microprocessor};
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(10,-1) (3) {Microphone};
  
  %\node[draw, rectangle, minimum width=2cm, minimum height=1cm] at(10,0) (4) {ADC};
  
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(10,-3.3) (5) {Motor};
  %\node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(10,-4) (6) {LED Driver};
  
  %\node[draw, rectangle, minimum width=3cm, minimum height=2.5cm] at(0,-4) (7) {Power Supply};
  
  \node[draw, rectangle, minimum width=3cm, minimum height=1.5cm] at(10,1) (8) {Speaker};
  
  \node[draw, rectangle, minimum width=2cm, minimum height=1cm] at(10,-4.8) (9) {Encoder};
  \node[draw, rectangle, minimum width=3.6cm, minimum height=3.3cm, dashed] at(10,-3.9) { };
  \node[draw, rectangle, minimum width=3.6cm, minimum height=4.4cm, dashed] at(10,0.2) { };
  \node at(10,2.1) {Ultrasonic Module};
  
  %\node[draw, rectangle, minimum width=2.5cm, minimum height=1.5cm, align=center] at(4.75,4.5) (10) {Temperature\\sensor};
  %\node[draw, rectangle, minimum width=2.5cm, minimum height=1.5cm, align=center] at(7.50,4.5) (11) {Humidity\\sensor};
  %\node[draw, rectangle, minimum width=2.5cm, minimum height=1.5cm, align=center] at(10.25,4.5) (12) {Pressure\\sensor};
  %\node[draw, rectangle, minimum width=8.5cm, minimum height=2.5cm, dashed] at(7.5,4.65) { };
  %\node at(7.5,5.6) {Calibration Sensors};
  %\draw[->] (5.25,3.25) -- (5.25,0.75);
  %\draw[->] (2.5,3.25) -- (2.5,2.5) -- (5,2.5) -- (5,0.75);
  %\draw[->] (-0.25,3.25) -- (-0.25,2) -- (4.75,2) -- (4.75,0.75);
 
  %\draw[->] (12) -- (10.25,2.8) -- (5.25,2.8) -- (5.25,0.75);
  %\draw[->] (11) -- (7.5,3) -- (5,3) -- (5,0.75);
  %\draw[->] (10) -- (4.75,0.75);
  
  
  \node[draw, rectangle, minimum width=9cm, minimum height=9cm] at(7.5,-1.3) { };
  \node at(7.5,2.7) {\Large{Sonar Range Scanner}};
  
  % PC -> FPGA
  \draw[->] (-0.2,2.25) -- (-0.2,0.75) node[above,midway] { };
  \draw[<-] (0.2,2.25) -- (0.2,0.75) node[above,midway] { };
  %\draw[<-] (1.5,-0.2) -- (3.5,-0.2) node[above,midway] { };
  
  % FPGA -> uC
  %\draw[<-] (1) -- (2) node[above,midway] { };
  \draw[->] (1.5,0.2) -- (3.5,0.2) node[above,midway] { };
  \draw[<-] (1.5,-0.2) -- (3.5,-0.2) node[above,midway] { };
  % uC -> Speaker
  \draw[->] (6.5,0.25) -- (7.5,0.25) -- (7.5,1)-- (8) node[above,midway] { };
  %uC -> Motor
  \draw[->] (5.25,-0.75) -- (5.25,-3.3)--(8.5,-3.3) node[above,midway] { };
  % Encoder -> uC
  \draw[->] (9,-4.8) -- (4.75,-4.8) -- (4.75,-0.75);
  % Microphone -> uC
  \draw[->] (3) -- (7.5,-1) -- (7.5,-0.25)-- (6.5,-0.25) node[above,midway] { };
\end{tikzpicture}
\caption{Block diagram of the non-automatic calibration sonar range scanner system.}\label{fig:non_auto_complete_sys_block_diagram}
\end{figure}
\end{comment}