#include <avr/io.h>
#include "adc.h"

void init_adc(){
	// Enable ADC (ADEN should be cleared before entering power saving)
	ADCSRA |= (1<<ADEN);


	/* ADC Prescaler Selections:
	 * ADPS2	ADPS1	ADPS0		Division Factor
	 * 0		0		0			2
	 * 0		0		1			2
	 * 0		1		0			4
	 * 0		1		1			8
	 * 1		0		0			16
	 * 1		0		1			32
	 * 1		1		0			64
	 * 1		1		1			128
	 * */


	/* ADC in Single Conversion Mode
	 * 1. Disable Power Reduction (Clear PRADC) -> PRR &= ~(1<<PRADC) ???
	 * 2. Set ADC Start Conversion bit -> ADCSRA |= (1<<ADSC)
	 *    2.1 Note: ADSC will automatically be cleared by hardware by end of conversion
	 *
	 * */


	/* ADC in Free Running Mode
	 * 1. Starting the series of conversion is done by setting the ADC Start Conversion bit -> ADCSRA |= (1<<ADSC)
	 *
	 * */

	/* ADC in Auto Trigger Mode
	 *
	 * ADC Auto Trigger Source
	 * ADTS2	ADTS1	ADTS0	Trigger Source
	 * 0		0		0		Free Running Mode
	 * 0		0		1		Analog Comparator
	 * 0		1		0		External Interrupt Request 0
	 * 0		1		1		Timer0 Compare Match A
	 * 1		0		0		Timer0 Overflow
	 * 1		0		1		Timer1 Compare Match B
	 * 1		1		0		Timer1 Overflow
	 * 1		1		1		Timer1 Capture Event
	 * */



	/* Reading from the ADC
	 * - ADC Interrupt Flag (ADIF) is set when data is ready
	 * - Result of A/D conversion ends up in ADCH and ADCL registers.
	 *   - Result is by default presented right adjusted, for left adjusted-> ADMUX |= (1<<ADLAR)
	 * - MUST read ADCL first then ADCH to ensure that data belongs together
	 *   - When ADCL is read this blocks the ADC's access to the registers
	 *   - When ADCH is read this releases the registers so the ADC can access them again
	 * */
}
