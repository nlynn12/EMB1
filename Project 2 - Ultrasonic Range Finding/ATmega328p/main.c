#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "timer/timer.h"
#include "timer/sw_timers.h"
#include "motor/motor.h"
#include "usart/usart.h"
#include "adc/adc.h"



// PROGRAM: sudo avrdude -pm328p -cusbasp -P/dev/ttyUSB0 -Uflash:w:ATmega328P_alive.hex:a

int alive_timer = TIM_500_MSEC;


char temp[] = "25 c\n";

int main(){
	cli();
	timer2_init();
	USART_init();
	PWM_init();
	sei();


	DDRB |= 0xFF;
	while(1){
		while(!ticks);
		// Following will be executed every 4ms
		ticks--;

		if(!--alive_timer){
			alive_timer = TIM_500_MSEC;
			PORTB ^= (1<<PB5);
			USART_putstring(temp);
		}
		set_duty_cycle(255);

	}
}
