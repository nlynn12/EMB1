/*
 *	University of Southern Denmark
 *
 *	Project:
 *	Module name:	sw_timers.h
 *
 *	Description:
 *
 *	------------------------------------------------------
 *	Change Log:
 *
 *	Date		ID			Change
 *	YYMMDD
 *
 *	150311		NAL			Module Created
 *
 *	------------------------------------------------------
 */


#ifndef _TIMER_H
#define _TIMER_H

/***************************** Include files *******************************/
#include <avr/io.h>


/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

volatile uint8_t ticks;

ISR (TIMER2_COMPA_vect) {
	ticks++;
}

void timer2_init() {
	/**
	 * Set up timer 0A to compare match mode
	 * (See section 15.9.1 and 15.9.2 from p.104 and forth in ATmega328P datasheet)
	 * */

	// CTC mode
	TCCR2A &= ~(1 << WGM00);
	TCCR2A |= 1 << WGM01;
	TCCR2B &= ~(1 << WGM02);

	/*	Prescaler	Timer Clock					Timer Period				Timer Value
	 * 	None		16 MHz						1/16 Mhz = 0.0625us			4ms / 0,0625 = 64000
	 * 	8			16 MHz/8 = 2 MHz			1/2 Mhz = 0.5us				4ms / 0.5 = 8000		<-- Selected
	 * 	64			16 MHz/64 = 250 kHz			1/250 khz = 4us				4ms / 4 = 1000
	 * 	256			16 MHz/256 = 62.5 kHz		1/62.5 khz = 16us			4ms / 16 = 250
	 * 	1024		16 MHz/1024 = 15.625 kHz	1/15.625 khz = 64us			4ms / 64 = 62.5
	 * */

	// Set timer to click at F_CPU / 8.
	TCCR2B |= (1 << CS01);

	// Compare with Timer Value (250-1)
	OCR2A = 249;

	// Enable timer
	TIMSK2 |= (1 << OCIE2A);

	// start interrupt counter at zero
	ticks = 0;
}
/****************************** End Of Module *******************************/
#endif /*_TIMER_H*/
