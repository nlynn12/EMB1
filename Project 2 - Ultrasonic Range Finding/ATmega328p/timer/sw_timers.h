/*
 *	University of Southern Denmark
 *
 *	Project:		Button events
 *	Module name:	sw_timers.h
 *
 *	Description:
 *
 *	------------------------------------------------------
 *	Change Log:
 *
 *	Date		ID			Change
 *	YYMMDD
 *
 *	150311		NAL			Module Created
 *
 *	------------------------------------------------------
 */


#ifndef _SW_TIMERS_H
#define _SW_TIMERS_H

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/
#define TIM_20_MSEC		160
#define TIM_50_MSEC		400
#define TIM_100_MSEC	800
#define TIM_200_MSEC	1600
#define TIM_500_MSEC	4000
#define TIM_1_SEC		8000
#define TIM_2_SEC		16000
#define TIM_10_SEC		80000

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

/****************************** End Of Module *******************************/
#endif /*_SW_TIMERS_H*/
