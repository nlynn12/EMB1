#ifndef USART_H
#define USART_H

#define F_CPU 16000000UL
#define BAUD 9600
#define BAUD_PRESCALER (((F_CPU / (BAUD * 16UL))) - 1)

void USART_init(void);
void USART_send(unsigned char data);
unsigned char USART_receive(void);
void USART_putstring(char* StringPtr);

#endif //USART_H
