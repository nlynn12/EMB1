#include <avr/io.h>

#ifndef MOTOR_H
#define MOTOR_H

void PWM_init();
void set_duty_cycle(uint8_t duty);

#endif //MOTOR_H
