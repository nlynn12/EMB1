#include <avr/io.h>
#include "motor.h"

void PWM_init(){
	/* COM0A1	COM0A2		Description
	 * 0		0			Normal operation
	 * 0		1			Toggle OC0A on compare match
	 * 1		0			Clear OC0A on compare match
	 * 1		1			Set OC0A on compare match
	 * */
	TCCR0A |= (1<<COM0A1)|(1<<WGM01)|(1<<WGM00);
	TCCR0B |= (1<<CS02);

	// Output port for PWM (PD6)
	DDRD |= (1<<PD6);
}

void set_duty_cycle(uint8_t duty){
	OCR0A = duty;
}
