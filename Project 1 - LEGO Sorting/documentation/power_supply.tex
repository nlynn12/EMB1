\section{Designing the Power Supply}
This project provides a $15V$ input, from which three different voltages and current outputs are required:

\begin{itemize}\itemsep-3pt
\item 5V/0.5A for the FPGA
\item 6V/1A for the RC servo
\item 12V/1.5A for the LED's
\end{itemize}

When designing a power supply, one has the choice between a linear and a switching regulator. For this project, a switching supply has been chosen for the FPGA, while the servo and LED's will be driven by linear power supplies (please see the project description in appendix \ref{app:projectdescription}).

\subsection{Switching Regulator}
The given switching regulator is the LM2574HV-5.0. Following the datasheet\footnote{\cite{TI:LM2574} p- 1}, the standard circuit for a buck converter, which steps down the voltage, can be seen on figure \ref{fig:FPGASupply} below:

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{img/FPGASupply}
\caption{Eagle$^{\scriptsize{\textregistered}}$ schematic of the switching regulated power supply for the FPGA.}
\label{fig:FPGASupply}
\end{figure}

A switching regulator works by storing energy in the magnetic fields of the inductor. When the charging stops, the coil, directed by the diode, unloads its energy into the filter capacitor and from there into the load. Some of the output is lead back into the regulator by a feedback loop. This is fed to the internal error amplifier, which compares it with a reference voltage. If the sample voltage is below the reference voltage, the amplifier output voltage is increased and vice versa. This output is fed to a PWM circuit which controls the on--time for the inductor based on this input.\footnote{\cite{practical_electronics} p. 710-713}

\subsubsection{Choice of Components for the Switching Regulator}
The inductor is selected based on figure \ref{fig:inductorSelection} from the datasheet\footnote{\cite{TI:LM2574} p. 14}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.8]{img/inductorSelection}
\caption{Inductor selection guide for LM2574HV-5.0.}
\label{fig:inductorSelection}
\end{figure}

It can be seen that with an input of $15V$ and a maximum load current of $0.5A$, a $330\mu$H inductor is needed.
According to the datasheet\footnote{\cite{TI:LM2574} p. 13}, the output capacitor has to have a value between $100\mu$F and $470\mu$F. In addition, it is recommended to have a voltage rating that is at least $1.5$ times greater than the output voltage. A capacitor of $330\mu$F, with a voltage rating of $25V$ has been chosen.
The fly-back diode's current rating must be at least $1.5$ times the maximum load current. Furthermore, the reverse voltage rating should be at least $1.25$ times the maximum input voltage. Here, a $1A$ Schottky diode with a reverse voltage rating of $20V$ has been chosen.
Finally, an input capacitor is needed for stable operation. A $22\mu$F capacitor with a $25V$ rating has been chosen, based on datasheet recommendation.

\newpage
\subsection{Linear Regulators}
This project calls for two linear regulators. One with a $6V$/$1A$ output and one with a $12V$/$1.5A$ output. Both are chosen from the LM78xx series. Their schematics, derived from the datasheets\footnote{\cite{ON_semiconductor:L7806CV} p. 7}\footnote{\cite{STMicroelectronics:NCP7812TG} p. 1}, can be seen on figure \ref{fig:RCLEDSupply}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{img/RCLEDSupply.png}
\caption{Eagle$^{\scriptsize{\textregistered}}$ schematic of the linear power supplies for the RC servo and the LED's.}
\label{fig:RCLEDSupply}
\end{figure}

A linear regulator works by controlling an internal voltage divider network in order to maintain a constant output voltage. This is done by feeding some of the output voltage back to an error amplifier, which compares this sample with fixed reference voltage. If the sample differs from the reference, the error amplifier will output the error, which will be fed to a transistor, which controls the load current.\footnote{\cite{practical_electronics} p. 700-701}

\subsubsection{Choice of Components for the Linear Regulators}
Following the datasheets\footnote{\cite{ON_semiconductor:L7806CV} p. 7}, the input capacitor is chosen to be a $0.33\mu F$, while the output capacitor, which improves transient response, is recommended to be $0.1\mu F$, so that is the chosen value.

The flyback diodes used are the same Schottky diodes used for the switching regulator. They have a peak surge current of $25A$, which is plenty. According to the datasheet\footnote{\cite{ON_semiconductor:L7806CV} p. 10}, a flyback diode is recommended in the case that the build--in diode would not be sufficient enough. The build in protection diode is rated to 5A in a few milliseconds, and as the desktop powersupply cannot source more than 2/3A then the external flyback diode is not needed. It is however (in a moment of weakness) implemented in the design.

\newpage
\subsection{PCB Design of the Power Supply}
The PCB has been designed in Eagle$^{\scriptsize{\textregistered}}$, where it is chosen to use a one-layer PCB with a ground plane. The ground plane is advised in order to provide the best heat transfer and to minimize voltage transients due to inductances\footnote{\cite{TI:LM2574} p. 12 and p. 20}. Furthermore, it makes routing easier, as a lot of pins require a ground connection. The PCB has a row of 14 pin headers at the bottom, whereof 6 are used. Two are used for ground, one for 15V input and three for 12V, 6V and 5V output. The redundant amount of pins are there for stability, when the PCB is plugged into a breadboard. Figure \ref{fig:powerSupplyPCB} shows the PCB design.

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth]{img/powerSupplyPCB.png}
\caption{Eagle$^{\scriptsize{\textregistered}}$ PCB design for the full power supply. Top to bottom: $5V$/$0.5A$ switching regulator, $6V$/$1A$ linear regulator, $12V$/$1.5A$ linear regulator.}
\label{fig:powerSupplyPCB}
\end{figure}

\section{Efficiency Test of the Power Supply}
An efficiency test of the power supply has been conducted and can be found in appendix \ref{app:power_supply_test}. From the table in this appendix it can be noted that the $12V$ linear power supply is the most efficient, at $75.82\%$. This is logical, as this supply performs the smallest voltage regulation. The second most efficient is the $5V$ supply at $71.45\%$. This supply is more efficient than the $6V$ linear supply at $38.40\%$, as it is a switching supply.
The datasheet\footnote{\cite{TI:LM2574} p. 4} for the switching regulator states that the typical efficiency with a $12V$ input and a load current of $0.5A$ is $77\%$. Therefore, an efficiency of $74.45\%$ with a $15V$ input is deemed satisfying.