For detecting which color the LEGO$^{\scriptsize{\textregistered}}$ brick is, a series of high power LED's and a single photodiode is used. For conversion of the analogue signals to digital ones the ADC MCP3008 is used. This ADC uses SPI communication to deliver the result of a A/D conversion. This is visualized in figure \ref{fig:complete_sys_block_diagram}.

\section{Interfacing the High Power LEDs with the FPGA}
The high power LED's are to be controlled from the FPGA. As the FPGA cannot supply the LED's due to their current consumption, a driver circuit for each LED color is needed. Furthermore, the LED's is chosen to be controlled with PWM signals, as pulsed control allows overdriving, and hereby a higher light intensity. The circuit can be seen in the schematic in figure \ref{fig:LEDDriver}.

\begin{figure}[h!]
\centering
\includegraphics[width=0.35\textwidth]{img/LEDDriver.png}
\caption{Eagle schematic of the LED driver used for the high power LED's.}
\label{fig:LEDDriver}
\end{figure}

The LED driver should be visualized as an ideal switch and could consist of e.g. a BJT or a MOSFET, where the choice fell on the NTD5867NL MOSFET\footnote{\cite{ON_semiconductor:NTD5867NL}}. It is an n--channel enhancement MOSFET, which means that, it is off when no difference in voltage exists between the gate and the source. This way, the LED's are off until a logic high signal from the FPGA is received at the gate. The fact that it is an n--channel MOSFET means that when it is on, current flows from the drain to the source. This way, the LED's are powered by the $12V$ power supply and they are controlled by the FPGA. Connected to the MOSFET's gate is two resistors, one input resistor and a pull--down resistor, used to drive the signal low when no input voltage is present. Furthermore, the MOSFET is chosen due to its high gate input impedance, which means that the transistor draws almost no current. It could be argued that the input resistor could be left out due to the MOSFET's high input impedance, however it is implemented for extra security.

\subsection{Overdriving the LEDs by PWM signals}
When LEDs are controlled with a PWM signal, the current through them can be significantly increased, causing them to emit light at much higher intensities than the ones gotten from regular DC driving. In application brief I-024\footnote{\cite{avago:application_brief}}, it is suggested that the optimum pulse frequency is 1kHz, however the strobing rate will be much higher in this application, and it will depend on the other modules timing characteristics.

In order to overdrive the LED's, there are two main factors to be aware of. The first is the peak drive current, $I_{\text{peak}}$, of the LED. This is the maximum current the LED can be driven with by pulsed operation. Connected to this factor is the maximum duty cycle, $D_{\text{max}}$, at which the LEDs can be driven at $I_{\text{peak}}$. Looking at the datasheets for the Red\footnote{\cite{Avago:red}}, Green and Blue\footnote{\cite{Avago:bluegreen}} LEDs, the values in table \ref{overdriveTable} are found.

\begin{table}[h!]
\centering
\def\arraystretch{1.2}%
\begin{tabular}{|l|l|l|l|}
\hline
            & Red & Green & Blue \\ \hline
$I_{\text{peak}}$ [mA] & 100 & 100   & 100  \\ \hline
$D_{\text{max}}$ [\%]  & 30  & 10    & 10   \\ \hline
\end{tabular}
\caption{Table of peak currents and duty cycles for LED overdriving.}\label{overdriveTable}
\end{table}

With three LEDs the desired duty cycle is 33.33\% as this allows for the simplification of the PWM module for the LEDs (shown in chapter \ref{chp:controller}). In application brief I-024\footnote{\cite{avago:application_brief} p. 2}, a graph illustrates the $I_{\text{peak}}$ current corresponding to a duty cycle. The graph is shown in figure \ref{fig:recommended_pwm_range}.

\begin{figure}[H]
 \centering
 \includegraphics[width=0.7\textwidth]{img/pwm_range}
 \caption{Recommended Range for Pulsed Drive Current Conditions for AllnGaP LEDs.}\label{fig:recommended_pwm_range}
\end{figure}

Here, with a duty cycle of 33.33\% the peak drive current is approximately 90mA which means that a current limiting resistor needs to be calculated. This is done so the current through them is 90mA. As seen in figure \ref{fig:LEDDriver}, two of each color LED are connected in series. The LED input voltage is 12V. An input resistor is needed in order to feed 90mA to the LEDs. In order to determine the resistor value, another lookup in the datasheets is required. From the datasheet for the red LED's, a maximum voltage drop over the diode of 2.3V can be read from the current/forward voltage graph. For the blue and green LED's it is 4.1V. From this, the resistor values is calculated below.
$$R_{\text{red}} = \frac{12V - 2.3V - 2.3V}{0.09A} = 82\ohm$$
$$R_{\text{blue/green}} = \frac{12V - 4.1V - 4.1V}{0.09A} = 42\ohm$$

\begin{comment}
\subsection{Tests}
In order to determine the improvement made by overdriving the LED's, the intensity of the light emitted from the PWM driven diodes has been compared to that of the DC driven ones. As the PWM driven diodes are rapidly turning on and off, the measurement of the intensity has to happen only when the LED's are on. The results from the test can be seen in table \ref{testTable} below:

\begin{table}[h!]
\centering

\begin{tabular}{|l|l|l|l|}
\hline
            & Red & Green & Blue \\ \hline
DC driven	& ??  & ??    & ??   \\ \hline
PWM driven  & ??  & ??    & ??   \\ \hline
\end{tabular}
\caption{Table of the measured emitted light from the diodes, both DC driven and PWM driven.}\label{testTable}
\end{table}

From the table it can be seen that overdriving the LED's increases the emitted light by ????.
The full test description can be found in appendix ???.
 
\end{comment}

\section{Interfacing the ADC with the FPGA}
The FPGA operates with a high logic level of 3.3V where the ADC can operate in the range of 2.7V to 5.5V\footnote{\cite{microchip:MCP3008}}. When selecting the source it is important to note that the max sampling rate is different for the two extremes, 75ksps and 200ksps respectively. It is chosen to power the ADC with 3.3V as this elemininates the need for logic level conversion between the FPGA and the ADC, and hereby saves external components.

If the ADC however did not have the ability to run at 3.3V then the logic level conversion could be done with a transistor or even a set of resistors and diodes.


\section{Interfacing the Photodiode with the ADC}
The photodiode generates electric current directly from light energy and is then said to operate in photovoltaic mode (i.e. it is not reverse biased). This small generated current needs to be converted to a larger voltage in order to give the ADC the ability to convert the analogue signals to digital ones. This can be done by using a standard operational amplifier (op amp) or an instrumental amplifier in a transimpedance amplifier circuit. For this project the single supply op amp LM2904 is used as it is a low cost component compared to the instrumental amplifiers available which typically are 10 times more expensive. The op amp does however not operate in rail-to-rail as an instrumental amplifier could. The LM2904 has a range from $0 - 3.7V$ ($V_{\text{sat}}$) when powered by 5V. It is however not needed that the op amp saturates at 3.7V when the ADC samples in the range of 0-3.3V. Hereby the op amp should indeed not be supplied by 5V but with 4.5V so the op amp saturates at 3.3V. In figure \ref{fig:current-to-voltage_converter} the diagram for the current--to--voltage converter circuit is shown with the photodiode mounted on the inverting input of the op amp.
\newpage
\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{img/photodiode.png}
\caption{Eagle schematic of the transimpedance amplifier circuit which constitutes the current--to--voltage converter.}
\label{fig:current-to-voltage_converter}
\end{figure}

In the figure it can be seen that the gain resistor is chosen to be variable. This is done so that when the final board containing the LED's and the photodiode has been designed and placed on the slider, the gain can be tuned in order to acquire the optimum range. The optimum range is where the known highest reflection (blue light on blue brick) just barely goes into saturation. For the gain resistor, a $200k\ohm$ trimmer potentiometer has been chosen based on the test in appendix \ref{app:raw_photodiode_output}.

In parallel with the gain resistor, a bypass capacitor is placed. This capacitor is used in order to reduce oscillations in the amplifier output. The size of this capacitor can be found by using the equation for an transimpedance operational amplifier however a simpler reasonable estimate is the equation for an RC filter.

$$f_c = \frac{1}{2\pi R_{F} C_{F}}$$

where $f_c$ is the cutoff frequency, $R_F$ is the feedback resistor and $C_F$ is the feedback capacitor. Next the $C_F$ isolated, and the equation becomes:

$$C_F = \frac{2\pi R_{F}}{f_c}$$

Before it is possible to ``plug in the numbers'', $R_F$ needs to be determined. This is done, as mentioned, by adjusting the potentiometer until the strongest color (blue) just barely goes into saturation when a (blue) brick is present. After the fine tuning, the resistor measurs $74$k$\ohm$. Then, the cutoff frequency is measured using an oscilloscope, where a frequency of $180$kHz is found. These values are inserted in the equation, which yields:

$$C_F = \frac{2\pi \cdot 72,000\ohm}{180,000\text{Hz}} = 2.58p\text{F}$$

\begin{comment}
The other equation is designed specifically for transimpedance filters:

$$C_F = \frac{1}{4\pi R_{F}f_{GBWP}}(1+\sqrt{1 + 8\pi R_{F}C_{i}f_{GBWP}})$$

Here, $f_GBWP$ is the unity gain bandwidth of the op amp which, is $HABLA$. $C_i$ is found to be $HABLA$. Then, $C_F$ can be found:

$$C_F = \frac{1}{4\pi 72000\ohm}f_{GBWP}}(1+\sqrt{1 + 8\pi 72000\ohm C_{i}f_{GBWP}}) = ??p\text{F}$$

It can be seen that BLABLABLA.
\end{comment}

Various capacitors from this value and up have been tested, until the choice fell on a $6.7p$F capacitor, as this was the one that cancelled out most of the oscillations.

\begin{comment}
\begin{figure}[h!]
 \begin{tikzpicture}[scale=1]
  \draw[color=black]
    (0,0) node[ground]{}
    (0,0) to [pDo] (0,2) -| (2,2)
    %(0,2) -- (2,2)
    %()
    
    (2, 2) node[op amp] (opamp) {}
    %(opamp.-) to[R] (-3, 0.5)
    (opamp.-) to[short,*-] ++(0,1.5) coordinate (leftC)
    to[C] (leftC -| opamp.out)
    to[short,-*] (opamp.out)
  ;
 \end{tikzpicture}
\end{figure}
\end{comment}


\newpage
\section{PCB Design for the Color Detection Module}
The PCB for the color detection module is designed as a stacked, two part, block with an $8$--pin connector in between. In figure \ref{fig:colordet} the two PCB's can be seen.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{img/top_bottom.png}
\caption{Eagle schematic of the top and bottom PCB's for the color detection module.}
\label{fig:colordet}
\end{figure}

It can be seen on the right handside board, that the LED's have been placed in a circle around the photodiode. Two of each color is used and they are placed opposite of each other for the best exposure. All LED's are soldered on slightly slanted towards the photodiode in order to achieve the best reflection angle.

By separating the board containing the ADC from the one containing the LEDs and photodiode, the design is somewhat generic. The whole module is connected to the FPGA through a ribbon cable which is placed in the breadboard in front of the FPGA and the power supply. So there exist almost none wire--breadboard connections.

\newpage
\section{SPI Communication between the FPGA and the ADC}\label{sec:SPICommunication}
The SPI communication is implemented as a generic module which can be reused in any other application. As it can be seen in figure \ref{fig:SPIMaster_module_diagram} the SPIMaster needs information as; clock phase (\textsf{CPHA}), clock polarity (\textsf{CPOL}), clock divisor (\textsf{CLK\_DIV}), etc. Futhermore a complete list of pin descriptions can be found in table \ref{tab:pin_description_spi_master}. Please note that this module is a development of the code developed by Scott Larson\footnote{Scott Larson's version of the SPI Master VHDL module can be obtained from this address: \url{https://eewiki.net/pages/viewpage.action?pageId=7569477\#SerialPeripheralInterface(SPI)Master(VHDL)-Features}}.

\begin{figure}[H]
\centering
 \begin{tikzpicture}[
    >=stealth',
    ]

 \node[draw, rectangle, minimum width=3cm, minimum height=6cm] at(9,0) (3) { };
 \node[draw, rectangle, minimum width=3cm, minimum height=6cm] at(14,0) (4) { };
 \node[draw, rectangle, minimum width=2.5cm, minimum height=2.5cm] at(18.5,0) (5) { };
 \node at (9,2.5) {User Logic};
 \node at (9,2) {(ADCDriver)};
 \node at (14,2.5) {SPI Master};
 \node at (18.5,0.8) {SPI Slave};
 \node at (18.5,0.3) {(ADC)};
 

 %ADCDriver to SPIMaster
% \draw[->] (10.5,2.35)--(12.5,2.35) node[right=2pt] {\scriptsize\textsf{CLK}} node[left=57pt]{...};
% \draw[->] (10.5,2)--(12.5,2) node[right=2pt] {\scriptsize\textsf{ADDR}} node[left=57pt]{...};
% \draw[->] (10.5,1.65)--(12.5,1.65) node[right=2pt] {\scriptsize\textsf{CPHA}} node[left=57pt]{...};
% \draw[->] (10.5,1.3)--(12.5,1.3) node[right=2pt] {\scriptsize\textsf{CPOL}} node[left=57pt]{...};
% \draw[->] (10.5,0.95)--(12.5,0.95) node[right=2pt] {\scriptsize\textsf{enable}} node[left=57pt]{...};
% \draw[<-] (10.5,0.6)--(12.5,0.6) node[right=2pt] {\scriptsize\textsf{busy}} node[left=57pt]{...};
 
 \draw[->] (10.5,1.15)--(12.5,1.15) node[right=2pt] {\scriptsize\textsf{CLK}} node[left=57pt]{...};
 %\draw[->] (10.5,1.15)--(12.5,1.15) node[right=2pt] {\scriptsize\textsf{ADDR}} node[left=57pt]{...};
 \draw[->] (10.5,0.8)--(12.5,0.8) node[right=2pt] {\scriptsize\textsf{CPHA}} node[left=57pt]{...};
 \draw[->] (10.5,0.45)--(12.5,0.45) node[right=2pt] {\scriptsize\textsf{CPOL}} node[left=57pt]{...};
 \draw[->] (10.5,0.1)--(12.5,0.1) node[right=2pt] {\scriptsize\textsf{enable}} node[left=57pt]{...};
 \draw[<-] (10.5,-0.25)--(12.5,-0.25) node[right=2pt] {\scriptsize\textsf{busy}} node[left=57pt]{...};
 
 \draw[->] (10.5,-0.6)--(12.5,-0.6)  node[right=2pt] {\scriptsize\textsf{CLK\_DIV}} node[left=57pt]{...};
 %\draw[->] (10.5,-0.95)--(12.5,-0.95)  node[right=2pt] {\scriptsize\textsf{CMD\_ENA}} node[left=57pt]{...};
 %\draw[->,very thick] (10.5,-1.3)--(12.5,-1.3)  node[right=2pt] {\scriptsize\textsf{MOSI\_CMD[N..0]}} node[left=57pt]{...};
 \draw[<-,very thick] (10.5,-0.95)--(12.5,-0.95) node[right=2pt] {\scriptsize\textsf{MISO[M..0]}} node[left=57pt]{...};
 \draw[->,very thick] (10.5,-1.3)--(12.5,-1.3) node[right=2pt] {\scriptsize\textsf{MOSI[M..0]}} node[left=57pt]{...};
 %\draw[{implies[length=5mm]}-,double distance=10pt] (12.49,-0.3)--(10.51,-0.3) node[midway] {\scriptsize\textsf{MOSI}};
 %\draw[{implies[length=5mm]}-,double distance=10pt] (10.51,-1.1)--(12.49,-1.1) node[midway] {\scriptsize\textsf{MISO}};
 %\draw (9.5,-1.5)--(9.5,-2);
 %\draw (9.5,-2) -- (14,-2) node[above=-2pt, midway] {\scriptsize\textsf{enable}};
 %\draw[->] (14,-2)--(14,-1.5);
 
 %SPIMaster ->
 \draw[->] (15.5,0.6) -- (17.25, 0.6)	 node[above=-3pt,midway] {\scriptsize $\overline{\textsf{CS}}$};
 \draw[->] (15.5,0.2) -- (17.25, 0.2)	 node[above=-3pt,midway] {\scriptsize \textsf{SCLK}};
 \draw[->] (15.5,-0.2) -- (17.25, -0.2)	 node[above=-3pt,midway] {\scriptsize \textsf{MOSI}};
 \draw[<-] (15.5,-0.6) -- (17.25, -0.6)	 node[above=-3pt,midway] {\scriptsize \textsf{MISO}};
 
 \end{tikzpicture}
 \caption{The module diagram shows the complete list of signals that the SPIMaster module needs.}
 \label{fig:SPIMaster_module_diagram}
\end{figure}

%\begin{savenotes}
\begin{table}[h!]
\hspace{-0.75cm}
  \begin{tabular}{p{2cm} >{\centering\arraybackslash}p{1.3cm} >{\centering\arraybackslash}p{1.3cm} p{4.5cm} p{6.5cm}}
   \toprule
   \textbf{Port}	& \textbf{Width}	& \textbf{Mode}	& \textbf{Data Type}	& \textbf{Description}\\\midrule
   CLK 			& 1 			& in 		& STD\_LOGIC 		& System clock\\
   %ADDR			& 32                    & in            & INTEGER               & Address of slave\\
   CPHA                 & 1                     & in            & STD\_LOGIC            & Clock phase of the SPI clock\\
   CPOL                 & 1                     & in            & STD\_LOGIC            & Clock polarity of the SPI clock\\
   enable               & 1                     & in            & STD\_LOGIC            & Enables a transmission\\
   busy                 & 1                     & out           & STD\_LOGIC            & Busy flag to control data flow\\
   CLK\_DIV             & 32                    & in            & INTEGER               & Clock divisor\\
   %CMD\_ENA             & 1                     & in            & STD\_LOGIC            & Boolean, if '1' MOSI\_CMD will be transmitted\\
   %MOSI\_CMD            & N                     & in            & STD\_LOGIC\_VECTOR    & Command buffer of size N set by user\\
   MOSI                 & M                     & in            & STD\_LOGIC\_VECTOR    & MOSI buffer of size M set by user\\
   MISO                 & M                     & out           & STD\_LOGIC\_VECTOR    & MISO buffer of size M set by user\\
   %$\overline{\text{CS}}$	& P			& inout         & STD\_LOGIC\_VECTOR    &  Array of chip's addresses of size P where P is the number of chips\\
   $\overline{\text{CS}}$	& 1			& out         & STD\_LOGIC    &  Chip's select\\
   \bottomrule
  \end{tabular}
 \caption{Pin descriptions of the SPI Master Module.}
 \label{tab:pin_description_spi_master}
\end{table}
%\end{savenotes}

This means that it is possible to use this module with any other device that runs the SPI protocol. It is possible to run in multiple different SPI modes at multiple different SPI clock speeds at the same time.

\subsection{Determining the Characteristics for the SPI Communication between the FPGA and the ADC}
As the ADC according to the datasheet\footnote{\cite{microchip:MCP3008}} can only run in SPI Modes 0,0 and 1,1 this means that writing of data occurs on the falling edge of the SPI clock while latching in data occurs on rising edge for both devices.

Furthermore the maximum SPI clock frequency is stated in the datasheet to be 3.6MHz. The clock divisor (\textsf{CLK\_DIV}) needs to be calculated which is done by looking at the system clock which is 50MHz. Here a clock period is 20ns where in the SPI clock a clock period is 277.78ns. It is desired to get a clock frequency as close to the maximum to achieve the fastest communication possible for the ADC. And as we want to have a clock signal (50\% duty cycle) we divide that by 2 so we can toggle our scaled down clock. This yields
$$\textsc{CLK\_DIV}=\text{ceil}\left(\nicefrac{\left(\frac{277.78ns}{20ns}\right)}{2}\right) = 7$$
In appendix \ref{chp:vhdl_sim} section \ref{sec:vhdl_sim:SPI} a simulation of the SPI Communication is shown. From this simulation the transmission time was read to be 5.02$\mu$s.



%
%\todo[inline]{What was our thoughts with this???}

