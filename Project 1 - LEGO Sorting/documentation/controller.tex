The controller is essentially a state machine with three states; idle, sampling, and deciding, where it starts in the sampling state. This is illustrated in figure \ref{fig:fsm_diagram}. In the sampling state one sample is taken for one of the three colored LEDs. It then switches state to the idle state where it checks if a sample has been conducted once for each color. If not, then it returns to take the next sample. If one sample has been conducted per color it switches state to the deciding state where the color of the LEGO Brick is determined. When this is done the \textsf{ServoDriver} described in chapter \ref{chp:rcservo}, recieves a position vector of 8 bits describing if the RC Servo should move left, center or right.

\begin{figure}[H]
\centering
 \begin{tikzpicture}[
    ->,>=stealth',
    node distance=6.5cm,
    ]

  \node[state,initial,accepting,minimum size=2cm] (1) {Sampling};
  \node[state,minimum size=2cm] (2) [right of=1] {Idle};
  \node[state,minimum size=2cm] (3) [right of=2] {Deciding};
 
 %\begin{pgfinterruptboundingbox}
\path[every node/.style={font=\sffamily\scriptsize}]
    (1) edge [bend left] node[above] {SampleDone/LED=off} (2)
    (2) edge [bend left] node[above=4pt] {LED=on/Samples += 1} (1)
    (2) edge [bend left] node[above] {Samples = 3/} (3)
    %    edge [loop above] node[above] {Press2/FuelSelect95} (2)
    %    edge [loop left] node [below,yshift=-8pt] {Press1/FuelSelect92} (2)
    %    edge [loop right] node [below,yshift=-8pt] {Press3/FuelSelectOC} (2)
    (3) edge [bend left] node[below] { } (1);
 %\end{pgfinterruptboundingbox}
 \end{tikzpicture}
 \caption{The state machine diagram illustrates the three controller states where 3 samples is taken, one for each color LED.}
 \label{fig:fsm_diagram}
\end{figure}

\section{The Sampling State}
As seen in the state machine diagram, figure \ref{fig:fsm_diagram}, the LED in question is turned on, in the whole sampling state, and is turned of in any other state. The sampling state uses the counter, \textsf{Samples}, to determine which of the three LEDs that should be turned on. This means that when \textsf{Samples} = 1 then LED 1 is turned on in the sampling state and LED 2 + 3 is turned off. Similar when \textsf{Samples} = 2 then LED 2 is turned on in the sampling state and LED 1 + 3 is turned off, etc. This results in a PWM signal with a dutycycle of 33.33\% for each LED. This is illustrated in figure \ref{fig:total_sample_time}.

\subsection{Determining the Optimal Sampling Time}
As the LED--Photodiode--Amplifier circuit consists of electronics with settling times it is desired to determine the worst case settling time to a step response (when the LED turns on). The question is if the circuit is faster than the SPI communication discussed in chapter \ref{chp:colordetection} or vice versa. In appendix \ref{app:worst_case_slew_rate}, six diagrams is shown, two for each color, with no brick and a brick in its respective color. Here the worst case settling time was the blue LED with no brick present and it was measured to be 13.6$\mu$s. Recall from the SPI communication section \ref{sec:SPICommunication} that the transmission time was 5.02$\mu$s which means that the LED--Photodiode--Amplifier circuit is the ``bottle neck'' of the system (it is the slowest component). To ensure that a sample conducted by the ADC will be taken at a optimal time, it is critical that the LED--Photodiode--Amplifier circuit has settled first. This means that the sampling state will run as the pseudo code in listing \ref{code:pseudo_sampling_state} below.

\begin{lstlisting}[language=pseudo,caption=Pseudo code for the Sampling State,label=code:pseudo_sampling_state]
 Turn on LED
 wait for 14us
 Enable SPICommunication
 wait for SPICommunication = done
 Turn of LED
 State = Idle
\end{lstlisting}


\section{The Idle State}
The idle state is responsible for keeping track of how many number of samples that has been conducted. When the number of samples is below 3, the sample counter is incremented and the idle state changes to the sampling state once again. When the number of samples reaches 3, as seen in figure \ref{fig:fsm_diagram}, the idle state knows that there has been conducted one sample for each of the three LEDs, and it changes state to the deciding state.

\section{The Deciding State}
In the deciding state the three values from the ADC is compared respectively to a set of three thresholds which has been determined in appendix \ref{app:thresholds}, and can be seen in table \ref{tab:thresholds}.

\begin{table}[H]
 \centering
 \begin{tabular}{l c r}
  \toprule
  \textbf{Color} & \textbf{Voltage} & \textbf{Numerical Value}\\
  \midrule
  Red & 1.2V & 372\\
  Green & 2.8V & 868\\
  Blue & 3.0V & 930\\
  \bottomrule
 \end{tabular}
 \caption{The table shows three thresholds for each of the three colors detectable by the system.}\label{tab:thresholds}

\end{table}


If an ADC value is higher than the respective threshold a '1' is set in a logic vector, else a '0' is set at its respective position. This logic vector is then used in a process which controlls the RC Servo and hereby which way each color brick should go.


\newpage
\section{Total time for a Single Sample}
The expected total sampling time for each sample should be
$$14\mu\text{s} + 5.02\mu\text{s} = 19.02\mu\text{s}$$
In figure \ref{fig:total_sample_time} the signals for turning on and off the three colored LEDs is shown. In Matlab the signal width is measured and it is approximately 19.04$\mu$s so it is deemed satisfying.

\begin{figure}[H]
 \centerline{\includegraphics[width=1.2\textwidth]{img/total_sample_time}}
 \caption{Diagram showing the signals going from the FPGA to the three LEDs. Furthermore the 33.33\% duty cycle of the LEDs is shown in the diagram. It is in this period, where the LED is lit, that the SPI communication happens. Approximately $\nicefrac{2}{3}$ in.}
 \label{fig:total_sample_time}
\end{figure}

It is now decided that the sequence of sampling three times, one for each color, from now on will be refered to as one sample$_{\text{RGB}}$. So the sample$_{\text{RGB}}$ period is (3 times 19.02$\mu$s) 57.06$\mu$s which yields that the system is able to produce (1s/57.06$\mu$s) 17.5 ksps.

