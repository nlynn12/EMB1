----------------------------------------------------------------------------------
-- Company:     University of Southern Denmark
-- Engineer:    Nicolai Anton Lynnerup
-- 
-- Create Date: 09/29/2015 11:17:02 AM
-- Design Name: 
-- Module Name: SPIMaster - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Attention:  This code is made with inspiration from:
--             https://eewiki.net/pages/viewpage.action?pageId=4096096
--
--
-- |  SPI Mode  |   CPOL  |  CPHA  |
-- |     0      |    0    |   1    |
-- |     1      |    0    |   0    |
-- |     2      |    1    |   1    |
-- |     3      |    1    |   0    |
--
--
-- When CPOL = 0, SCLK idles to LOW
--    → For CPHA = 0:   Data is:    Output on falling edge
--                                  Captured on rising edge
--    → For CPHA = 1:   Data is:    Output on rising edge
--                                  Captured on falling edge
-- When CPOL = 1, SCLK idles to HIGH
--    → For CPHA = 0:   Data is:    Output on rising edge
--                                  Captured on falling edge
--    → For CPHA = 1:   Data is:    Output on falling edge
--                                  Captured on rising edge
--
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity SPIMaster is
    Port (  CLK_50MHz   : in STD_LOGIC;
            SCLK        : out STD_LOGIC;
            MOSI        : out STD_LOGIC;
            MISO        : in STD_LOGIC;
            CS_N        : out STD_LOGIC;
            CPHA        : in STD_LOGIC;
            CPOL        : in STD_LOGIC;
            CLK_DIV     : in INTEGER;
            enable      : in STD_LOGIC;
            busy        : out STD_LOGIC;
            MISO_O      : out STD_LOGIC_VECTOR(9 downto 0));
end SPIMaster;

architecture Behavioral of SPIMaster is
    -- state machine signals
    type spi_state is (idle, execute);
    signal state: spi_state;
    
    signal clock_toggle : integer range 0 to 36;
    signal clock_count  : integer range 0 to 7;
    --constant clock_div  : integer := 7;
    signal scaled_clock : STD_LOGIC := '0';
    
    constant MOSI_data  : STD_LOGIC_VECTOR(16 downto 0) := "11000000000000000";
    signal MOSI_buffer  : STD_LOGIC_VECTOR(16 downto 0) := "00000000000000000";
    signal MISO_buffer  : STD_LOGIC_VECTOR(9 downto 0)  := "0000000000";
    
    --constant CPHA       : STD_LOGIC := '0';
    --constant CPOL       : STD_LOGIC := '0';
    signal assert_d     : STD_LOGIC;
    
begin
    process(CLK_50MHz)
    begin
        if(CLK_50MHz'event and CLK_50MHz = '1') then
            case state is
                when idle =>
                    busy <= '0';
                    MOSI <= 'X';
                    CS_N <= '1';
                    MISO_buffer   <= (others => '0');
                    if enable = '1' then
                        busy          <= '1';       -- Set busy flag
                        CS_N          <= '0';       -- Select chip
                        scaled_clock  <= CPOL;      -- Set sclk polarity
                        assert_d      <= not(CPHA); -- Set sclk phase
                        clock_count   <= 1;         -- Start clock counter
                        MOSI_buffer   <= MOSI_data;
                        state <= execute;           -- State change
                    else
                        state <= idle;              -- Stay in idle
                    end if;
                 when execute =>
                    if clock_count = CLK_DIV then
                        clock_count <= 1;
                        
                        -- Toggle data asserter --
                        assert_d <= not(assert_d);
                        
                        -- Toggle SCLK --
                        scaled_clock <= not(scaled_clock);
                        clock_toggle <= clock_toggle + 1;
                        
                        -- MOSI --
                        if assert_d = '0' then
                            MOSI <= MOSI_buffer(16);
                            MOSI_buffer <= MOSI_buffer(15 downto 0) & '0';
                        end if;
                        
                        -- MISO --
                        if assert_d = '1' and clock_toggle >= 15 and clock_toggle <= 34 then
                            MISO_buffer <= (MISO_buffer(8 downto 0) & MISO);
                        end if;
                        
                        -- EOT --
                        if clock_toggle >= 34 then
                            busy          <= '0';
                            CS_N          <= '1';
                            MOSI          <= 'X';
                            state         <= execute;
                            if clock_toggle = 36 then
                                clock_toggle <= 0;
                                scaled_clock  <= CPOL;
                                state <= idle;
                            end if;
                        end if;
                    else
                        clock_count <= clock_count + 1;
                        state <= execute;
                    end if;
            end case;
        end if;
    end process;
    
    SCLK <= scaled_clock;
    MISO_O <= MISO_buffer;

end Behavioral;