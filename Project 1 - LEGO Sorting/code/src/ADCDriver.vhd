----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/31/2015 08:16:39 AM
-- Design Name: 
-- Module Name: ADCDriver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADCDriver is
    Port (  CLK_50MHz   : in  STD_LOGIC;
            SCLK        : out STD_LOGIC;
            MOSI        : out STD_LOGIC;
            MISO        : in  STD_LOGIC;
            CS_N        : out STD_LOGIC;
            enable      : in  STD_LOGIC;
            busy        : out STD_LOGIC;
            MISO_O      : out STD_LOGIC_VECTOR(9 downto 0));
end ADCDriver;

architecture Behavioral of ADCDriver is
    ----------------------------------------------------------------------
    --                            SPIMaster                             --
    ----------------------------------------------------------------------
    component SPIMaster is
        Port (  CLK_50MHz   : in  STD_LOGIC;
                SCLK        : out STD_LOGIC;
                MOSI        : out STD_LOGIC;
                MISO        : in  STD_LOGIC;
                CS_N        : out STD_LOGIC;
                CPHA        : in STD_LOGIC;
                CPOL        : in STD_LOGIC;
                CLK_DIV     : in INTEGER;
                enable      : in  STD_LOGIC;
                busy        : out STD_LOGIC;
                MISO_O      : out STD_LOGIC_VECTOR(9 downto 0));
    end component SPIMaster;
    
    signal CPHA        : STD_LOGIC := '0';
    signal CPOL        : STD_LOGIC := '0';
    signal CLK_DIV     : INTEGER := 7;
    
begin

    SPIMasterMap : SPIMaster
    port map(   CLK_50MHz=>CLK_50MHz,
                SCLK=>SCLK,
                MOSI=>MOSI,
                MISO=>MISO,
                CS_N=>CS_N,
                CPHA=>CPHA,
                CPOL=>CPOL,
                CLK_DIV=>CLK_DIV,
                enable=>enable,
                busy=>busy,
                MISO_O=>MISO_O);

    process(CLK_50MHz)
    begin
        if CLK_50MHz'event and CLK_50MHz = '1' then
            CPHA <= '0';
            CPOL <= '0';
            CLK_DIV <= 7;
        end if;
    end process;

end Behavioral;
