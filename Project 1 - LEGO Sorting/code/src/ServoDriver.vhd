----------------------------------------------------------------------------------
-- Company:     University of Southern Denmark
-- Engineer:    Nicolai Anton Lynnerup
-- 
-- Create Date: 09/28/2015 07:53:37 AM
-- Design Name: 
-- Module Name: ServoDriver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Revision 0.02 - FSM Created
-- Revision 1.00 - FSM Deleted and replaced with more dynamic code
--                 Now the driver supports positioning
-- Additional Comments:
--      Attention:  This code is made with inspiration from:
--                  https://www.hackmeister.dk/2010/07/controlling-an-rc-servo-with-an-fpga/
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL; 

entity ServoDriver is
    Port ( CLK_50MHz : in STD_LOGIC;
           Position : in STD_LOGIC_VECTOR (7 downto 0);
           Servo : out STD_LOGIC);
end ServoDriver;

architecture Behavioral of ServoDriver is

    constant ClockDivisor: integer := 196; -- 255kHz
    
    signal ClockTick: STD_LOGIC := '0';
    signal ClockCount: STD_LOGIC_VECTOR (7 downto 0) := "00000000";
    
    signal PulseCount: STD_LOGIC_VECTOR (11 downto 0) := "000000000000";
    
begin

    process(CLK_50MHz)
    begin
        if CLK_50MHz = '1' and CLK_50MHz'event then
            if ClockCount = ClockDivisor then
                ClockTick <= '1';
            else
                ClockTick <= '0';
            end if;
            if ClockTick = '1' then
                ClockCount <= "00000000";
            else
                ClockCount <= ClockCount + 1;
            end if;
        end if;
    end process;

    process(CLK_50Mhz) -- Runs on sys clk as you must not run on gated clocks
    begin
        if CLK_50MHz = '1' and CLK_50MHz'event then
            if ClockTick = '1' then    
                PulseCount <= STD_LOGIC_VECTOR(unsigned(PulseCount + 1));     
            end if;
            if PulseCount < ("0001" & Position) then
                Servo <= '1';
            else
                Servo <= '0';
            end if;
        end if;
    end process;

end Behavioral;