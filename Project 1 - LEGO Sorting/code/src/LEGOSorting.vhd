----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/31/2015 08:16:39 AM
-- Design Name: 
-- Module Name: LEGOSorting - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LEGOSorting is
    Port ( CLK_50MHZ   : in    STD_LOGIC;
           PWM_servo   : out   STD_LOGIC;
           PWM_led     : out   STD_LOGIC_VECTOR(2 downto 0);
           SCLK        : out   STD_LOGIC;
           MOSI        : out   STD_LOGIC;
           MISO        : in    STD_LOGIC;
           CS_N        : out   STD_LOGIC;
           LED         : out   STD_LOGIC_VECTOR(9 downto 0);
           TN_SERIAL_O : out   STD_LOGIC;
           TN_SERIAL_I : in    STD_LOGIC);
end LEGOSorting;

architecture Behavioral of LEGOSorting is
    
    Component Controller is
    Generic ( N_LEDS : integer := 3);
    Port (
        CLK_50MHZ   : in    STD_LOGIC;
        PWM_servo   : out   STD_LOGIC;
        PWM_led     : out   STD_LOGIC_VECTOR(N_LEDS-1 downto 0);
        SCLK        : out   STD_LOGIC;
        MOSI        : out   STD_LOGIC;
        MISO        : in    STD_LOGIC;
        CS_N        : out   STD_LOGIC;
        LED         : out   STD_LOGIC_VECTOR(9 downto 0);
        TN_SERIAL_O : out   STD_LOGIC;
        TN_SERIAL_I : in    STD_LOGIC);
    End Component Controller;

    
begin
    ControllerMap : Controller
    port map(
        CLK_50MHZ,
        PWM_servo,
        PWM_led,
        SCLK, 
        MOSI,
        MISO,     
        CS_N,
        LED,
        TN_SERIAL_O,
        TN_SERIAL_I 
    );

end Behavioral;
