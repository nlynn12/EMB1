----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/24/2015 08:59:26 PM
-- Design Name: 
-- Module Name: LegoSorting - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Controller is
    Generic ( N_LEDS : integer := 3);
    Port (
        CLK_50MHZ   : in    STD_LOGIC;
        PWM_servo   : out   STD_LOGIC;
        PWM_led     : out   STD_LOGIC_VECTOR(N_LEDS-1 downto 0);
        SCLK        : out   STD_LOGIC;
        MOSI        : out   STD_LOGIC;
        MISO        : in    STD_LOGIC;
        CS_N        : out   STD_LOGIC;
        LED         : out   STD_LOGIC_VECTOR(9 downto 0);
        TN_SERIAL_O : out   STD_LOGIC;
        TN_SERIAL_I : in    STD_LOGIC);
end Controller;

architecture Behavioral of Controller is
    ----------------------------------------------------------------------
    --                            Servo Motor                           --
    ----------------------------------------------------------------------
    component ServoDriver is
        Port (  CLK_50MHz   : in STD_LOGIC;
                Position    : in STD_LOGIC_VECTOR (7 downto 0);
                Servo       : out STD_LOGIC);
    end component;

    signal servo_pos    : STD_LOGIC_VECTOR(7 downto 0) := "10000000";
    
    ----------------------------------------------------------------------
    --                            ADCDriver                             --
    ----------------------------------------------------------------------
    component ADCDriver is
        Port (  CLK_50MHz   : in  STD_LOGIC;
                SCLK        : out STD_LOGIC;
                MOSI        : out STD_LOGIC;
                MISO        : in  STD_LOGIC;
                CS_N        : out STD_LOGIC;
                enable      : in  STD_LOGIC;
                busy        : out STD_LOGIC;
                MISO_O      : out STD_LOGIC_VECTOR(9 downto 0));
    end component ADCDriver;
    
    signal SPIBusy      : STD_LOGIC;
    signal SPIEnable    : STD_LOGIC;
    signal MISO_buffer  : STD_LOGIC_VECTOR(9 downto 0);

    
    ----------------------------------------------------------------------
    --                             uTosNet                              --
    ----------------------------------------------------------------------
    component PseudoTosNet_ctrl is
        Port (
            T_clk_50M				      : in	STD_LOGIC;
            T_serial_out				  : out STD_LOGIC;
            T_serial_in                   : in  STD_LOGIC;
            T_reg_ptr					  : out std_logic_vector(2 downto 0);
            T_word_ptr					  : out std_logic_vector(1 downto 0);
            T_data_to_mem				  : in  std_logic_vector(31 downto 0);
            T_data_from_mem				  : out std_logic_vector(31 downto 0);
            T_data_from_mem_latch		  : out std_logic);
    end component;
    
    --Used to connect to the Pseudo TosNet Controller component  
    signal T_reg_ptr                 : std_logic_vector(2 downto 0);
    signal T_word_ptr                : std_logic_vector(1 downto 0);
    signal T_data_to_mem             : std_logic_vector(31 downto 0);
    signal T_data_from_mem           : std_logic_vector(31 downto 0);
    signal T_data_from_mem_latch     : std_logic;
    
    ----------------------------------------------------------------------
    --                            Controller                            --
    ----------------------------------------------------------------------
    type controller_state is (idle, sampling, deciding);
    signal state : controller_state := sampling;
    
    signal clock_count : integer := 0;
        
    signal LEDAddress : integer range 0 to N_LEDS-1 := 0;
    
    signal RedData      : STD_LOGIC_VECTOR(9 downto 0) := "0000000000";
    signal GreenData    : STD_LOGIC_VECTOR(9 downto 0) := "0000000000";
    signal BlueData     : STD_LOGIC_VECTOR(9 downto 0) := "0000000000";
    
    signal VoteRed      : STD_LOGIC := '0';
    signal VoteGreen    : STD_LOGIC := '0';
    signal VoteBlue     : STD_LOGIC := '0';
    
    constant r_threshold : STD_LOGIC_VECTOR(9 downto 0) := "0101110100"; -- 1.2V
    constant g_threshold : STD_LOGIC_VECTOR(9 downto 0) := "1101100100"; -- 2.8V 
    constant b_threshold : STD_LOGIC_VECTOR(9 downto 0) := "1110100010"; -- 3,0V
    
    signal LEDColor : STD_LOGIC_VECTOR(2 downto 0) := "000";
    
    constant servo_left     : STD_LOGIC_VECTOR(7 downto 0) := "01011111";
    constant servo_right    : STD_LOGIC_VECTOR(7 downto 0) := "10010110";
    constant servo_center   : STD_LOGIC_VECTOR(7 downto 0) := "10000000";
    
begin
    ServoDriverMap : ServoDriver
    port map(   CLK_50MHz,
                servo_pos,
                PWM_servo);

    ADCDriverMap : ADCDriver
    port map(   CLK_50MHz=>CLK_50MHz,
                SCLK=>SCLK,
                MOSI=>MOSI,
                MISO=>MISO,
                CS_N=>CS_N,
                enable=>SPIEnable,
                busy=>SPIBusy,
                MISO_O=>MISO_buffer);
            
    PseudoTosNet_ctrlInst : PseudoTosNet_ctrl
    Port map (  T_clk_50M          => CLK_50MHz,
                T_serial_out       => TN_SERIAL_O,
                T_serial_in        => TN_SERIAL_I,
                T_reg_ptr		     => T_reg_ptr,					
                T_word_ptr		     => T_word_ptr,									
                T_data_to_mem	     => T_data_to_mem,					
                T_data_from_mem	     => T_data_from_mem,						
                T_data_from_mem_latch => T_data_from_mem_latch);
                    
    Controller: process(CLK_50MHz)
    begin
        if CLK_50MHz'event and CLK_50MHz = '1' then
            case state is
                when idle =>
                    PWM_led(LEDAddress) <= '0';
                    
                    clock_count <= 0;
                    
                    -- LED Change + State change --
                    if LEDAddress = N_LEDS-1 then
                        LEDAddress <= 0;
                        state <= deciding;
                    else
                        LEDAddress <= LEDAddress + 1;
                        state <= sampling;
                    end if;
   
                when sampling =>
                    PWM_led(LEDAddress) <= '1';
                    
                    clock_count <= clock_count + 1;
                    
                    -- Enable SPI at time XX--
                    if clock_count = 700 and SPIBusy = '0' then
                        SPIEnable <= '1';
                    else
                        SPIEnable <= '0';
                    end if;
                    
                    -- When SPI idles go back to idle state --
                    if clock_count > 850 and SPIBusy = '0' then
                        PWM_led(LEDAddress) <= '0';
                        
                        if LEDAddress = 0 then
                            RedData <= MISO_buffer;
                        elsif LEDAddress = 1 then
                            GreenData <= MISO_buffer;
                        else
                            BlueData <= MISO_buffer;
                        end if;
                                                
                        state <= idle;
                        -- DEBUG
                        LED <= MISO_buffer;
                    else
                        PWM_led(LEDAddress) <= '1';
                        state <= sampling;
                        -- DEBUG
                        LED <= (others => '0');
                    end if;
                    
                 when deciding =>
                    if RedData > r_threshold then
                        VoteRed <= '1';
                    else
                        VoteRed <= '0';
                    end if;
                    if GreenData > g_threshold then
                        VoteGreen <= '1';
                    else
                        VoteGreen <= '0';
                    end if;
                    if BlueData > b_threshold then
                        VoteBlue <= '1';
                    else
                        VoteBlue <= '0';
                    end if;
                    state <= sampling;
            end case;
        end if;
    end process Controller;
    
    process(CLK_50MHz)
    begin
        if CLK_50MHz'event and CLK_50MHz = '1' then
            case LEDColor is
                when "100" =>
                    servo_pos <= servo_left;
                when "010" =>
                    servo_pos <= servo_center;
                when "001" =>
                    servo_pos <= servo_right;
                when others =>
                
            end case;
        end if;
    end process;
    
    LEDColor <= VoteRed & VoteGreen & VoteBlue;

    ----------------------------------------------------------
    -- uTosNet to place data on the controller bus. FPGA=>PC
    ----------------------------------------------------------
    DatToTosNet:
    process(T_reg_ptr,T_word_ptr)
    begin
        T_data_to_mem<="00000000000000000000000000000000";	-- default data
        case (T_reg_ptr & T_word_ptr) is                   -- The addresses are concatenated for compact code
            when "00000" =>	T_data_to_mem(9 downto 0) <= RedData;
            when "00001" =>	T_data_to_mem(9 downto 0) <= GreenData;
            when "00010" =>	T_data_to_mem(9 downto 0) <= BlueData;
            when others =>
        end case;		
    end process;
    
    
end Behavioral;