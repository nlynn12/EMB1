----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/28/2015 08:24:43 AM
-- Design Name: 
-- Module Name: rc_servo_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ServoDriver_tb is
end ServoDriver_tb;

architecture Behavioral of ServoDriver_tb is

    component ServoDriver is
        Port ( CLK_50MHz : in STD_LOGIC;
           Position : in STD_LOGIC_VECTOR (7 downto 0);
           Servo : out STD_LOGIC);
    end component;
    
    signal CLK      : STD_LOGIC := '0';
    signal pos      : STD_LOGIC_VECTOR(7 downto 0) := "00000000";
    signal ServoOut : STD_LOGIC := '0';

        
    -- Clock
    constant CLK_period : time := 20ns;

begin
    UUT : ServoDriver port map(CLK_50MHz=>CLK, Position=>pos, Servo=>ServoOut);
    
    CLK_LABEL : process
       begin
           CLK <= '0';
           wait for CLK_period/2;
           CLK <= '1';
           wait for CLK_period/2;
       end process;

    SERVO : process
    begin
        pos <= "11111111";
        wait for 50ms;
        pos <= "01001110";
        wait for 50ms;
    end process;

end Behavioral;
