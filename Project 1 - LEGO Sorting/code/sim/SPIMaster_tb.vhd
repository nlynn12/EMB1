----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/29/2015 09:34:02 PM
-- Design Name: 
-- Module Name: SPIMaster_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SPIMaster_tb is
end SPIMaster_tb;

architecture Behavioral of SPIMaster_tb is

    component SPIMaster is
        Generic (
            datawidth       : integer := 10;
            commandwidth    : integer := 7;
            n_slaves        : integer := 1);
        Port ( CLK_50MHz    : in STD_LOGIC;                                 -- System clock
               CS_N         : inout STD_LOGIC_VECTOR(n_slaves-1 downto 0);    -- SPI: List of slaves
               SCLK         : out STD_LOGIC;                                -- SPI: Clock
               MOSI         : out STD_LOGIC;                                -- SPI: Master Out Slave In line
               MISO         : in STD_LOGIC;                                 -- SPI: Master In Slave Out line
               CPHA         : in STD_LOGIC;                                 -- SPI: Clock PHAse
               CPOL         : in STD_LOGIC;                                 -- SPI: Clock POLarity
               enable       : in STD_LOGIC;                                 -- Start communication
               CMD_ENA      : in STD_LOGIC;                                 -- Enable the use of commands via MOSI
               MOSICmd      : in STD_LOGIC_VECTOR(commandwidth-1 downto 0); -- Commands for slave
               ADDR         : in INTEGER;                                   -- "Address" of slave (wire number)
               CLK_DIV      : in INTEGER;                                   -- Clock divisor factor
               Busy         : out STD_LOGIC;                                -- Busy flag
               MISOReg      : out STD_LOGIC_VECTOR(datawidth-1 downto 0);   -- Recieved data 
               MOSIReg      : in STD_LOGIC_VECTOR(datawidth-1 downto 0));   -- Transmit data
    end component;
    
    signal commandwidth: integer := 7;
    signal datawidth: integer := 10;
    
    signal CLK      : STD_LOGIC := '0';
    signal SS       : STD_LOGIC := '0';
    
    signal miso     : STD_LOGIC := '1';
    
    signal cpha     : STD_LOGIC := '0';
    signal cpol     : STD_LOGIC := '0';
    
    signal enable_cmd   : STD_LOGIC := '1';
    signal MOSI_Cmd     : STD_LOGIC_VECTOR(commandwidth-1 downto 0) := "1100000";
    
    signal MOSI_Reg     : STD_LOGIC_VECTOR(datawidth-1 downto 0) := "1010101010";
    
    signal ENA          : STD_LOGIC := '0';
    
    
    signal address      : INTEGER := 0;
    signal divisor      : INTEGER := 6;
    -- Clock
    constant CLK_period : time := 20ns;

begin
    UUT : SPIMaster port map(
        CLK_50MHz=>CLK,
        SCLK=>SS,
        MISO=>miso,
        CPHA=>cpha,
        CPOL=>cpol,
        CMD_ENA => enable_cmd,
        MOSICmd => MOSI_Cmd,
        MOSIReg => MOSI_Reg,
        enable => ENA,
        ADDR => address,
        CLK_DIV => divisor);
    
    CLK_LABEL : process
    begin
       CLK <= '0';
       wait for CLK_period/2;
       CLK <= '1';
       wait for CLK_period/2;
    end process;
    
    
    SPI_LABEL : process
    begin
        wait for 100ns;
        ENA <= '1';
        wait for 50ns;
        ENA <= '0';
        wait for 1ms; 
    end process;

end Behavioral;
