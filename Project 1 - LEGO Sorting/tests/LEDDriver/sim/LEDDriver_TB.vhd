----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/23/2015 09:57:01 AM
-- Design Name: 
-- Module Name: LEDDriver_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LEDDriver_TB is
end LEDDriver_TB;

architecture Behavioral of LEDDriver_TB is
    component LEDDriver is
        Generic ( n_leds    : integer := 4);
        Port ( CLK_50MHz    : in STD_LOGIC;
               DutyCycle    : in STD_LOGIC_VECTOR(7 downto 0);
               CLK_DIV      : in INTEGER; -- max value should not exceed 65535
               ADDR         : in INTEGER; -- should not exceed n_leds-1
               PWM          : out STD_LOGIC_VECTOR(n_leds-1 downto 0));
    end component LEDDriver;
    
    signal CLK      : STD_LOGIC := '0';
    signal duty     : STD_LOGIC_VECTOR(7 downto 0) := "10000000";
    signal divisor  : integer := 96;
    signal wire     : integer := 0;
    signal pwm_out  : STD_LOGIC_VECTOR(3 downto 0) := "0000";  
    
begin
UUT : LEDDriver port map(CLK_50MHz=>CLK, DutyCycle=>duty, CLK_DIV=>divisor,ADDR=>wire,PWM=>pwm_out);

CLK_LABEL : process
   begin
       CLK <= '0';
       wait for 20ns;
       CLK <= '1';
       wait for 20ns;
   end process;

LED : process
begin
    wire <= 0;
    duty <= "01001011";
    wait for 5ms;
    wire <= 1;
    duty <= "00011001";
    wait for 5ms;
end process;


end Behavioral;
