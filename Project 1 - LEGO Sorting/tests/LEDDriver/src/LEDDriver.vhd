----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/23/2015 08:50:10 AM
-- Design Name: 
-- Module Name: LEDDriver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LEDDriver is
    Generic ( n_leds    : integer := 4);
    Port ( CLK_50MHz    : in STD_LOGIC;
           DutyCycle    : in STD_LOGIC_VECTOR(7 downto 0);
           CLK_DIV      : in INTEGER;-- range 0 to 65535;
           ADDR         : in INTEGER;-- range 0 to n_leds-1;
           PWM          : out STD_LOGIC_VECTOR(n_leds-1 downto 0));
end LEDDriver;

architecture Behavioral of LEDDriver is    
    signal ClockTick    : STD_LOGIC := '0';
    signal ClockCount   : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
    signal PulseCount   : STD_LOGIC_VECTOR (7 downto 0) := "00000000";
    signal sel_led      : INTEGER := 0;
begin
    process(CLK_50MHz)
    begin
        if CLK_50MHz = '1' and CLK_50MHz'event then
            if ClockCount = CLK_DIV then
                ClockTick <= '1';
            else
                ClockTick <= '0';
            end if;
            if ClockTick = '1' then
                ClockCount <= (others => '0');
            else
                ClockCount <= ClockCount + 1;
            end if;
        end if;
    end process;
    
    process(CLK_50Mhz)
    begin
        if CLK_50MHz = '1' and CLK_50MHz'event then
            sel_led <= ADDR;
            PWM <= (others => '0');
            if ClockTick = '1' then    
                PulseCount <= STD_LOGIC_VECTOR(unsigned(PulseCount + 1));
            end if;
            if PulseCount < DutyCycle then
                PWM(sel_led) <= '1';
            else
                PWM(sel_led) <= '0';
            end if;
        end if;
    end process;

end Behavioral;
