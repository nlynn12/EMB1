----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/23/2015 12:06:01 PM
-- Design Name: 
-- Module Name: ControllerLED - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ControllerLED is
    Port (  CLK_50MHz : in STD_LOGIC;
            PWM       : out STD_LOGIC_VECTOR(3 downto 0)
    );
end ControllerLED;

architecture Behavioral of ControllerLED is
    component LEDDriver is
        Generic ( n_leds    : integer := 4);
        Port ( CLK_50MHz    : in STD_LOGIC;
               DutyCycle    : in STD_LOGIC_VECTOR(7 downto 0);
               CLK_DIV      : in INTEGER;-- range 0 to 65535;
               ADDR         : in INTEGER;-- range 0 to n_leds-1;
               PWM          : out STD_LOGIC_VECTOR(n_leds-1 downto 0));
    end component LEDDriver;
    
    signal duty      : STD_LOGIC_VECTOR(7 downto 0);
    signal divisor   : integer;
    signal ad        : integer;
    
begin
    LEDDriver_map : LEDDriver port map(CLK_50MHz=>CLK_50MHz, DutyCycle=>duty,CLK_DIV=>divisor,ADDR=>ad, PWM=>PWM);

    process(CLK_50MHz)
    begin
        if CLK_50MHz = '1' and CLK_50MHz'event then
            duty <= "01001011";
            divisor <= 96;
            ad <= 0;
        end if;
    end process;

end Behavioral;
