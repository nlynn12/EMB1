----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/24/2015 10:31:51 AM
-- Design Name: 
-- Module Name: ADCDriver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADCDriver is
    Port ( CLK_50MHz : in STD_LOGIC;
           LED       : out STD_LOGIC_VECTOR(9 downto 0);
           MISO      : in STD_LOGIC;
           MOSI      : out STD_LOGIC;
           SCLK      : out STD_LOGIC;
           CS_N      : inout STD_LOGIC_VECTOR(0 downto 0)
           );
end ADCDriver;

architecture Behavioral of ADCDriver is
    component SPIMaster is
    Generic (
        datawidth       : integer := 10;
        commandwidth    : integer := 7;
        n_slaves        : integer := 1);
    Port ( CLK_50MHz    : in STD_LOGIC;                                 -- System clock
           CS_N         : inout STD_LOGIC_VECTOR(n_slaves-1 downto 0);    -- SPI: List of slaves
           SCLK         : out STD_LOGIC;                                -- SPI: Clock
           MOSI         : out STD_LOGIC;                                -- SPI: Master Out Slave In line
           MISO         : in STD_LOGIC;                                 -- SPI: Master In Slave Out line
           CPHA         : in STD_LOGIC;                                 -- SPI: Clock PHAse
           CPOL         : in STD_LOGIC;                                 -- SPI: Clock POLarity
           enable       : in STD_LOGIC;                                 -- Start communication
           CMD_ENA      : in STD_LOGIC;                                 -- Enable the use of commands via MOSI
           MOSICmd      : in STD_LOGIC_VECTOR(commandwidth-1 downto 0); -- Commands for slave
           ADDR         : in INTEGER;                                   -- "Address" of slave (wire number)
           CLK_DIV      : in INTEGER;                                   -- Clock divisor factor
           Busy         : out STD_LOGIC;                                -- Busy flag
           MISOReg      : out STD_LOGIC_VECTOR(datawidth-1 downto 0);   -- Recieved data 
           MOSIReg      : in STD_LOGIC_VECTOR(datawidth-1 downto 0));   -- Transmit data
    end component SPIMaster;
    
    signal CPHA         : STD_LOGIC;                                 -- SPI: Clock PHAse
    signal CPOL         : STD_LOGIC;                                 -- SPI: Clock POLarity
    signal enable       : STD_LOGIC;                                 -- Start communication
    signal CMD_ENA      : STD_LOGIC;                                 -- Enable the use of commands via MOSI
    signal MOSICmd      : STD_LOGIC_VECTOR(6 downto 0); -- Commands for slave
    signal ADDR         : INTEGER;                                   -- "Address" of slave (wire number)
    signal CLK_DIV      : INTEGER;                                   -- Clock divisor factor
    signal Busy         : STD_LOGIC;                                 -- Busy flag
    signal MISOReg      : STD_LOGIC_VECTOR(9 downto 0);    -- Recieved data 
    signal MOSIReg      : STD_LOGIC_VECTOR(9 downto 0);    -- Transmit data

begin
    SPIMasterMap : SPIMaster
    port map(   CLK_50MHz,
                CS_N,
                SCLK,
                MOSI,
                MISO,
                CPHA,
                CPOL,
                enable,
                CMD_ENA,
                MOSICmd,
                ADDR,
                CLK_DIV,
                Busy,
                MISOReg,
                MOSIReg);

    process(CLK_50MHz)
    begin
        if CLK_50MHz = '1' and CLK_50MHz'event then
            if Busy = '0' then
                enable <= '1';
                CPHA <= '0';
                CPOL <= '0';
                ADDR <= 0;
                CLK_DIV <= 7;
                
                CMD_ENA <= '1';
                MOSICmd <= "1100000";
                MOSIReg <= "1010101010";
            else
                enable <= '0';
            end if;
        end if;
    end process;

    LED <= MISOReg;

end Behavioral;
