----------------------------------------------------------------------------------
-- Company:     University of Southern Denmark
-- Engineer:    Nicolai Anton Lynnerup
-- 
-- Create Date: 09/29/2015 11:17:02 AM
-- Design Name: 
-- Module Name: SPIMaster - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
--
--
-- |  SPI Mode  |   CPOL  |  CPHA  |
-- |     0      |    0    |   1    |
-- |     1      |    0    |   0    |
-- |     2      |    1    |   1    |
-- |     3      |    1    |   0    |
--
--
-- When CPOL = 0, SCLK idles to LOW
--    → For CPHA = 0:   Data is:    Output on falling edge
--                                  Captured on rising edge
--    → For CPHA = 1:   Data is:    Output on rising edge
--                                  Captured on falling edge
-- When CPOL = 1, SCLK idles to HIGH
--    → For CPHA = 0:   Data is:    Output on rising edge
--                                  Captured on falling edge
--    → For CPHA = 1:   Data is:    Output on falling edge
--                                  Captured on rising edge
--
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SPIMaster is
    Generic (
        datawidth       : integer := 10;
        commandwidth    : integer := 7;
        n_slaves        : integer := 1);
    Port ( CLK_50MHz    : in STD_LOGIC;                                 -- System clock
           CS_N         : inout STD_LOGIC_VECTOR(n_slaves-1 downto 0);    -- SPI: List of slaves
           SCLK         : out STD_LOGIC;                                -- SPI: Clock
           MOSI         : out STD_LOGIC;                                -- SPI: Master Out Slave In line
           MISO         : in STD_LOGIC;                                 -- SPI: Master In Slave Out line
           CPHA         : in STD_LOGIC;                                 -- SPI: Clock PHAse
           CPOL         : in STD_LOGIC;                                 -- SPI: Clock POLarity
           enable       : in STD_LOGIC;                                 -- Start communication
           CMD_ENA      : in STD_LOGIC;                                 -- Enable the use of commands via MOSI
           MOSICmd      : in STD_LOGIC_VECTOR(commandwidth-1 downto 0); -- Commands for slave
           ADDR         : in INTEGER;                                   -- "Address" of slave (wire number)
           CLK_DIV      : in INTEGER;                                   -- Clock divisor factor
           Busy         : out STD_LOGIC;                                -- Busy flag
           MISOReg      : out STD_LOGIC_VECTOR(datawidth-1 downto 0);   -- Recieved data 
           MOSIReg      : in STD_LOGIC_VECTOR(datawidth-1 downto 0));   -- Transmit data
end SPIMaster;

architecture Behavioral of SPIMaster is
    -- State machine
    type controller_state is (idle, execute);
    signal state : controller_state;
    
    signal clk_counter  : INTEGER;
    signal clk_ratio    : INTEGER;
    signal clk_toggles  : INTEGER range 0 to datawidth*2+1;
    signal sel_slave    : INTEGER;
    signal assert_data  : STD_LOGIC;
    signal MOSI_buffer  : STD_LOGIC_VECTOR(datawidth-1 downto 0);
    signal MISO_buffer  : STD_LOGIC_VECTOR(datawidth-1 downto 0);
    signal MOSI_CMD     : STD_LOGIC_VECTOR(commandwidth-1 downto 0);
    signal scaled_clk   : STD_LOGIC;
    
begin
    process(CLK_50MHz)
    begin
        if(rising_edge(CLK_50MHz)) then
            case state is
                when idle =>
                    Busy  <= '0';
                    MOSI  <= 'Z';
                    CS_N  <= (others => '1');
                    if(enable = '1') then
                        Busy <= '1';
                        if(ADDR < n_slaves) then
                            sel_slave <= ADDR;
                        else
                            sel_slave <= 0;     -- defaults to first slave
                        end if;
                        scaled_clk  <= CPOL;      -- Set clock polarity
                        assert_data <= not(CPHA); -- Set clock phase
                        -- Initiate SPI CLK
                        clk_ratio   <= CLK_DIV;
                        clk_counter <= CLK_DIV;
                        
                        MOSI_CMD <= MOSICmd;
                        MOSI_buffer <= MOSIReg;   -- Load TX register into buffer
                        clk_toggles <= 0;       -- Reset CLK toggles
                        state <= execute;         -- State change
                    else
                        state <= idle;            -- Stay in idle mode
                    end if;
                when execute =>
                    Busy <= '1';
                    CS_N(sel_slave) <= '0';
                    -- Scale SYS CLK to SPI CLK (SCLK)
                    if(clk_counter = clk_ratio) then
                        clk_counter <= 1;
                        assert_data <= not(assert_data);
                        
                        -- Count # of SCLK toggles
                        if(clk_toggles = ((datawidth+commandwidth)*2)+1) then
                            clk_toggles <= 0;
                        else
                            clk_toggles <= clk_toggles + 1;
                        end if;
                        
                        -- Toggle SCLK
                        if(clk_toggles <= (datawidth+commandwidth)*2 and CS_N(sel_slave) = '0') then
                            scaled_clk <= not(scaled_clk);
                        end if;
                        
                        --MISO
                        if(assert_data = '0' and CS_N(sel_slave) = '0' and clk_toggles > commandwidth*2) then
                            MISO_buffer <= MISO_buffer(datawidth-2 downto 0) & MISO;
                        end if;
                        
                        --MOSI
                        if(assert_data = '1') then
                            if(clk_toggles < commandwidth*2 and CMD_ENA = '1') then
                                -- Send commands
                                MOSI <= MOSI_CMD(commandwidth-1);
                                MOSI_CMD <= MOSI_CMD(commandwidth-2 downto 0) & '0';
                            else
                                -- Send actual data
                                MOSI <= MOSI_buffer(datawidth-1);
                                MOSI_buffer <= MOSI_buffer(datawidth-2 downto 0) & '0';
                            end if;
                        end if;
                        
                        --EOT
                        if(clk_toggles = ((datawidth+commandwidth)*2)+1) then
                            Busy <= '0';
                            CS_N <= (others => '1');
                            MOSI <= 'Z';
                            MISOReg <= MISO_buffer;
                            state <= idle;
                        else
                            state <= execute;
                        end if;      
                    else
                        clk_counter <= clk_counter + 1;
                        state <= execute;
                    end if;
                    --Busy <= '0';
                    --state <= idle;
            end case;
        end if;
    end process;
    
    SCLK <= scaled_clk;

end Behavioral;
