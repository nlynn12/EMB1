----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/24/2015 07:20:43 PM
-- Design Name: 
-- Module Name: ADCDriver_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADCDriver_TB is
end ADCDriver_TB;

architecture Behavioral of ADCDriver_TB is

    component ADCDriver is
    Port ( CLK_50MHz : in STD_LOGIC;
           LED       : out STD_LOGIC_VECTOR(9 downto 0);
           MISO      : in STD_LOGIC;
           MOSI      : out STD_LOGIC;
           SCLK      : out STD_LOGIC;
           CS_N      : inout STD_LOGIC_VECTOR(0 downto 0)
           );
    end component ADCDriver;

    
    signal LED       : STD_LOGIC_VECTOR(9 downto 0);
    signal MISO      : STD_LOGIC;
    signal MOSI      : STD_LOGIC;
    signal SCLK      : STD_LOGIC;
    signal CS_N      : STD_LOGIC_VECTOR(0 downto 0);
    
    -- Clock
    signal CLK : STD_LOGIC := '0';
    constant CLK_period : time := 20ns;
begin
    UUT : ADCDriver port map(
        CLK, LED, MISO, MOSI, SCLK, CS_N
        );

    CLK_LABEL : process
    begin
       CLK <= '0';
       wait for CLK_period/2;
       CLK <= '1';
       wait for CLK_period/2;
    end process;

end Behavioral;
