----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/22/2015 08:29:34 AM
-- Design Name: 
-- Module Name: Controller - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Controller is
    Port ( CLK_50MHz : in STD_LOGIC;
           Input     : in STD_LOGIC;
           LED1   : out STD_LOGIC;
           LED2   : out STD_LOGIC;
           PWM       : out STD_LOGIC);
end Controller;

architecture Behavioral of Controller is
    type state_type is (s0,s1,s2);
    signal cr_state, nx_state : state_type;
    
    signal Position         : STD_LOGIC_VECTOR (7 downto 0) := "00000000";
    signal debounced_res    : STD_LOGIC;

    component ServoDriver is
        Port ( CLK_50MHz : in STD_LOGIC;
               Position : in STD_LOGIC_VECTOR (7 downto 0);
               Servo : out STD_LOGIC);
    end component ServoDriver;
    
    component debounce is
        Port (
            CLK     : in STD_LOGIC;
            input   : in STD_LOGIC;
            res     : out STD_LOGIC);
    end component debounce;
    
begin
    servo_mapping : ServoDriver
    port map(CLK_50MHz, Position, PWM);
    
    debounce_mapping : debounce
    port map(CLK_50MHz, Input, debounced_res);
    
    process (CLK_50MHz)
        begin
            if(rising_edge(CLK_50MHz)) then
                -- state change
                if(cr_state /= nx_state) then
                    cr_state <= nx_state;
                end if;
            end if;  
    end process;
    
    -- state machine process
    process(debounced_res, cr_state)    
        begin
        if (rising_edge(debounced_res)) then
            case cr_state is
                when s0 =>
                    Position <= "01011111";
                    LED1 <= '1';
                    LED2 <= '0';
                    nx_state <= s1;
                when s1 =>
                    Position <= "10000000";
                    LED1 <= '0';
                    LED2 <= '1';                  
                    nx_state <= s2;
                when s2 =>
                    Position <= "10010110";
                    LED1 <= '1';
                    LED2 <= '1';   
                    nx_state <= s0;
            end case;
         end if;
    end process;    

end Behavioral;