----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/11/2015 09:13:45 AM
-- Design Name: 
-- Module Name: debounce - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity debounce is
    Port (
        CLK     : in STD_LOGIC;
        input   : in STD_LOGIC;
        res     : out STD_LOGIC);
end debounce;

architecture Behavioral of debounce is
    signal flipflop     : STD_LOGIC_VECTOR(1 downto 0); -- flipflop 1 & 2
    signal cclr         : STD_LOGIC;                    -- counter clear (if '1' => clear | elsif '0' => increment)
    signal cout         : STD_LOGIC_VECTOR(19 downto 0) := (others => '0');                    -- counter out

begin
    cclr <= flipflop(0) xor flipflop(1);
    
    process(CLK)
    begin
        if(CLK'EVENT and CLK='1') then
            flipflop(1) <= flipflop(0);
            flipflop(0) <= input;
            if(cclr = '1') then
                cout <= (others => '0');
            elsif(cout(19) = '0') then
                cout <= cout + 1;
            else
                res <= flipflop(1);
            end if;
        end if;
    end process;

end Behavioral;
